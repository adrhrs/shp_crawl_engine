package main

import (
	"log"

	pg "github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

func (a *App) InsertProductKeyBulk(data []ProductMasterKey) (err error) {

	err = a.DB.Insert(&data)
	if err != nil {
		log.Println(err)
	}
	return
}

func (a *App) UpdateProductKeyBulk(data []ProductMasterKey) (err error) {

	err = a.DB.Update(&data)
	if err == pg.ErrMultiRows {
		err = nil
	} else if err != nil {
		log.Println(err)
	}

	return
}

func (a *App) GetAllProductKeys() (data []ProductMasterKey, err error) {

	err = a.DB.Model(&data).Select()
	if err != nil {
		return
	}

	return
}

func (a *App) GetDistinctShopID() (shopIDs []int, err error) {

	_, err = a.DB.Query(&shopIDs, getDistinctShopIDs)
	if err != nil {
		log.Println(err)
		return
	}

	return
}

func (a *App) GetUnDetailedProductKeys() (data []ProductMasterKey, err error) {

	_, err = a.DB.Query(&data, getUnDetailedProductKeys)
	if err != nil {
		log.Println(err)
		return
	}

	return
}

func (a *App) GetVasProductCrawl() (data []VasProductCrawl, err error) {

	err = a.DB.Model(&data).Select()
	if err != nil {
		return
	}

	return
}

func (a *App) GetVasProductCrawlByPID(productID int) (data VasProductCrawl, err error) {

	filter := func(q *orm.Query) (*orm.Query, error) {
		q = q.Where("product_id = ?", productID)
		return q, nil
	}

	err = a.DB.Model(&data).Apply(filter).First()
	if err != nil {
		return
	}

	return
}

func (a *App) InsertVasProductCrawlHistoryBulk(data []VasProductCrawlHistory) (err error) {

	err = a.DB.Insert(&data)
	if err != nil {
		log.Println(err)
	}
	return
}

func (a *App) GetVasProductCrawlHistoryByProductID(productID int) (data []VasProductCrawlHistory, err error) {

	_, err = a.DB.Query(&data, getVasProductCrawlHistoriesOrdered, productID)
	if err != nil {
		log.Println(err)
		return
	}

	return
}

func (a *App) InsertSellerProductCountBulk(data []SellerProductCount) (err error) {

	err = a.DB.Insert(&data)
	if err != nil {
		log.Println(err)
	}
	return
}

func (a *App) GetSellerProductCount(crawlStatus, offset, limit int) (data []SellerProductCount, err error) {

	filter := func(q *orm.Query) (*orm.Query, error) {
		q = q.Where("product_crawled = ?", crawlStatus)
		return q, nil
	}

	err = a.DB.Model(&data).Apply(filter).Order("id ASC").Limit(limit).Offset(offset).Select()
	if err != nil {
		return
	}

	return
}

func (a *App) UpdateSellerProductCount(data []SellerProductCount) (err error) {

	err = a.DB.Update(&data)
	if err == pg.ErrMultiRows {
		err = nil
	} else if err != nil {
		return
	}

	return
}

func (a *App) InsertProductsV2(data []ProductV2) (err error) {

	err = a.DB.Insert(&data)
	if err != nil {
		return
	}
	return
}
