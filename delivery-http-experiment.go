package main

import (
	"encoding/json"
	"fmt"
	"net"
	"net/http"
	"strconv"
)

func (a *App) PSGetSearchResultHandler(w http.ResponseWriter, r *http.Request) {

	var (
		isDownload bool
		err        error
		res        Response
		data       []PSSearchResult
	)

	defer func() {

		if isDownload && err == nil {
			csv := handleCSV(data)
			WriteCSVFile(w, csv, "product_watch_list.csv")
		} else {
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(res)
		}

	}()

	productIDs, err := DecodeToIntArr(r.FormValue("product_id"))
	if err != nil {
		res.Message = err.Error()
		return
	}

	isHistory, err := strconv.ParseBool(r.FormValue("is_history"))
	if err != nil {
		res.Message = err.Error()
		return
	}

	triggerCron, err := strconv.ParseBool(r.FormValue("trigger_cron"))
	if err != nil {
		res.Message = err.Error()
		return
	}

	isDownload, err = strconv.ParseBool(r.FormValue("is_download"))
	if err != nil {
		res.Message = err.Error()
		return
	}

	data, err = a.PSGetSearchResult(productIDs, isHistory, triggerCron)
	if err != nil {
		res.Message = err.Error()
		return
	}

	res.Message = "done"
	res.Data = data

}

func (a *App) MasterProductProjectHandler(w http.ResponseWriter, r *http.Request) {

	res := Response{}

	source := r.FormValue("source")
	if source == "" {
		res.Message = "source is required"
		return
	}

	level := r.FormValue("level")
	if level == "" {
		res.Message = "level is required"
		return
	}

	data := a.MasterProductProject(source, level)

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	res.Data = data
	return
}

func (a *App) MasterProductProjectHandlerV3(w http.ResponseWriter, r *http.Request) {

	res := Response{}

	procedure := r.FormValue("procedure")
	if procedure == "" {
		res.Message = "procedure is required"
		return
	}

	go a.MainCategoryBasedCrawlV3(procedure)
	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()
	res.Data = "check log, async task"
	return
}

func (a *App) SearchAPIHammerTestHandler(w http.ResponseWriter, r *http.Request) {

	res := Response{}
	worker, _ := strconv.Atoi(r.FormValue("worker"))
	if worker == 0 {
		res.Message = "worker is required"
		return
	}

	limit, _ := strconv.Atoi(r.FormValue("limit"))
	if worker == 0 {
		res.Message = "limit is required"
		return
	}

	keyword := r.FormValue("keyword")
	if keyword == "" {
		res.Message = "keyword is required"
		return
	}
	data := a.SearchAPIHammerTest(worker, limit, keyword)

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	res.Data = data
	return
}

func (a *App) HeavyHammerTestHandler(w http.ResponseWriter, r *http.Request) {

	res := Response{}

	numJobs, _ := strconv.Atoi(r.FormValue("num_jobs"))
	if numJobs == 0 {
		res.Message = "num_jobs is required"
		return
	}

	freq, _ := strconv.Atoi(r.FormValue("freq"))
	if freq == 0 {
		res.Message = "freq is required"
		return
	}

	minWorker, _ := strconv.Atoi(r.FormValue("min_worker"))
	if minWorker == 0 {
		res.Message = "min_worker is required"
		return
	}

	maxWorker, _ := strconv.Atoi(r.FormValue("max_worker"))
	if maxWorker == 0 {
		res.Message = "max_worker is required"
		return
	}

	keyword := r.FormValue("keyword")
	if keyword == "" {
		res.Message = "keyword is required"
		return
	}

	a.HeavyHammerTest(freq, minWorker, maxWorker, numJobs, keyword)
	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	res.Message = "done, check the log"
	return

}

func (a *App) BrandListHandler(w http.ResponseWriter, r *http.Request) {

	res := Response{}

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	data, msg, err := a.BrandList()
	if err != nil {
		res.Message = err.Error()
		return
	}

	res.Message = msg
	res.Data = data

	return
}

func (a *App) pingDetailHandler(w http.ResponseWriter, r *http.Request) {

	res := Response{}

	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		fmt.Fprintf(w, "userip: %q is not IP:port", r.RemoteAddr)
	}

	userIP := net.ParseIP(ip)
	if userIP == nil {
		fmt.Fprintf(w, "userip: %q is not IP:port", r.RemoteAddr)
		return
	}

	worker, _ := strconv.Atoi(r.FormValue("worker"))
	if worker == 0 {
		res.Message = "worker is required"
		return
	}

	limit, _ := strconv.Atoi(r.FormValue("limit"))
	if worker == 0 {
		res.Message = "limit is required"
		return
	}

	data := a.pingDetail(worker, limit)

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	res.Data = data
	return
}

func (a *App) CategoryListHandler(w http.ResponseWriter, r *http.Request) {

	res := Response{}

	data := a.CategoryList()

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	res.Data = data
	return
}

func (a *App) RouteSearchHandler(w http.ResponseWriter, r *http.Request) {

	res := FullSearchResp{}

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	q := r.URL.Query().Encode()
	res = a.doRouteSearch(q)

	return

}
