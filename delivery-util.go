package main

import (
	"encoding/csv"
	"fmt"
	"net/http"
	"strconv"
	"strings"
)

type (
	Header struct {
		ProcessTime float64  `json:"process_time"`
		Messages    []string `json:"messages"`
		Reason      string   `json:"reason"`
		ErrorCode   string   `json:"error_code"`
	}

	DefaultResponse struct {
		Header Header      `json:"header"`
		Data   interface{} `json:"data"`
	}
)

// WriteCSVFile is a wrapper for serving csv file over http response
func WriteCSVFile(w http.ResponseWriter, records [][]string, filename string) {
	contentDisposition := fmt.Sprintf("attachment;filename=%s", filename)
	w.Header().Set("Content-Type", "text/csv")
	w.Header().Set("Content-Disposition", contentDisposition)

	// expose content disposition to get filename on client
	w.Header().Set("Access-Control-Expose-Headers", "Content-Disposition")

	wr := csv.NewWriter(w)
	err := wr.WriteAll(records)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

//DecodeToIntArr decode str to int arr
func DecodeToIntArr(str string) (out []int, err error) {
	if str == "" {
		return
	}
	strArr := strings.Split(str, ",")
	for _, s := range strArr {
		id, errParse := strconv.Atoi(s)
		if errParse != nil {
			err = errParse
			return
		}
		out = append(out, id)
	}
	return
}

func handleCSV(res []PSSearchResult) (data [][]string) {

	data = [][]string{
		[]string{
			"product_id",
			"name",
			"url",
			"source",
			"position",
			"cp_id",
			"cp_name",
			"cp_url",
			"crawl_time",
		},
	}

	for _, r := range res {
		for _, p := range r.Histories {
			d := []string{
				strconv.Itoa(r.Product.ProductID),
				r.Product.ProductTitle,
				r.Product.ProductURL,
				"shopee",
				strconv.Itoa(p.ItemOrder),
				strconv.FormatInt(p.ItemID, 10),
				p.ItemName,
				p.ItemURL,
				p.UpdateTime.Format("2006-01-02 15:04:05"),
			}
			data = append(data, d)
		}

	}

	return

}
