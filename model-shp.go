package main

import "time"

type ShpRes struct {
	TotalCount      int           `json:"total_count"`
	Error           interface{}   `json:"error"`
	Items           []Item        `json:"items"`
	ReservedKeyword string        `json:"reserved_keyword"`
	HintKeywords    []interface{} `json:"hint_keywords"`
}

type Item struct {
	ItemID         int64   `json:"itemid"`
	ShopID         int     `json:"shopid"`
	Catid          int     `json:"catid"`
	Name           string  `json:"name"`
	Price          float64 `json:"price"`
	Stock          int     `json:"stock"`
	ViewCount      int     `json:"view_count"`
	HistoricalSold int     `json:"historical_sold"`
	ItemRating     struct {
		RatingStar        float64 `json:"rating_star"`
		RatingCount       []int   `json:"rating_count"`
		RcountWithImage   int     `json:"rcount_with_image"`
		RcountWithContext int     `json:"rcount_with_context"`
	} `json:"item_rating"`
	LikedCount int `json:"liked_count"`
	TimeStart  time.Time
	TimeFinish time.Time
	Order      int
}
type Detail struct {
	Item     ItemDetail  `json:"item"`
	Version  string      `json:"version"`
	Data     interface{} `json:"data"`
	ErrorMsg interface{} `json:"error_msg"`
	Error    interface{} `json:"error"`
}

type ItemDetail struct {
	Itemid           int64  `json:"itemid"`
	Name             string `json:"name"`
	Brand            string `json:"brand"`
	ItemStatus       string `json:"item_status"`
	IsSlashPriceItem bool   `json:"is_slash_price_item"`
	Condition        int    `json:"condition"`
	Categories       []struct {
		DisplayName string `json:"display_name"`
		Catid       int    `json:"catid"`
	} `json:"categories"`
	Images                 []string `json:"images"`
	Description            string   `json:"description"`
	NormalStock            int      `json:"normal_stock"`
	Stock                  int      `json:"stock"`
	Price                  float64  `json:"price"`
	PriceMinBeforeDiscount float64  `json:"price_min_before_discount"`
	PriceBeforeDiscount    float64  `json:"price_before_discount"`
	PriceMax               float64  `json:"price_max"`
	PriceMin               float64  `json:"price_min"`
	Discount               string   `json:"discount"`
	RawDiscount            int      `json:"raw_discount"`
	HistoricalSold         int      `json:"historical_sold"`
	Sold                   int      `json:"sold"`
	ItemRating             struct {
		RatingStar        float64 `json:"rating_star"`
		RatingCount       []int   `json:"rating_count"`
		RcountWithImage   int     `json:"rcount_with_image"`
		RcountWithContext int     `json:"rcount_with_context"`
	} `json:"item_rating"`
	LikedCount     int    `json:"liked_count"`
	CmtCount       int    `json:"cmt_count"`
	ViewCount      int    `json:"view_count"`
	Shopid         int    `json:"shopid"`
	IsOfficialShop bool   `json:"is_official_shop"`
	ShopLocation   string `json:"shop_location"`
}

type SellerInfo struct {
	Version string `json:"version"`
	Data    struct {
		CampaignConfig                        interface{} `json:"campaign_config"`
		RatingNormal                          int         `json:"rating_normal"`
		Followed                              interface{} `json:"followed"`
		Userid                                int         `json:"userid"`
		Shopid                                int         `json:"shopid"`
		ShowOfficialShopLabelInNormalPosition interface{} `json:"show_official_shop_label_in_normal_position"`
		RatingBad                             int         `json:"rating_bad"`
		Mtime                                 int         `json:"mtime"`
		HasDecoration                         bool        `json:"has_decoration"`
		IsShopeeVerified                      bool        `json:"is_shopee_verified"`
		CancellationRate                      int         `json:"cancellation_rate"`
		IsOfficialShop                        bool        `json:"is_official_shop"`
		Vacation                              bool        `json:"vacation"`
		ShowLowFulfillmentWarning             bool        `json:"show_low_fulfillment_warning"`
		ItemCount                             int         `json:"item_count"`
		IsAbTest                              bool        `json:"is_ab_test"`
		PreparationTime                       int         `json:"preparation_time"`
		FollowerCount                         int         `json:"follower_count"`
		CbOption                              int         `json:"cb_option"`
		ShopLocation                          string      `json:"shop_location"`
		EnableDisplayUnitno                   bool        `json:"enable_display_unitno"`
		Status                                int         `json:"status"`
		IsBlockingOwner                       interface{} `json:"is_blocking_owner"`
		Description                           string      `json:"description"`
		RatingGood                            int         `json:"rating_good"`
		IsSemiInactive                        interface{} `json:"is_semi_inactive"`
		ShowOfficialShopLabel                 bool        `json:"show_official_shop_label"`
		ShopCovers                            []struct {
			Type     int    `json:"type"`
			ImageURL string `json:"image_url"`
			VideoURL string `json:"video_url"`
		} `json:"shop_covers"`
		ShowOfficialShopLabelInTitle bool `json:"show_official_shop_label_in_title"`
		CampaignID                   int  `json:"campaign_id"`
		BuyerRating                  struct {
			RatingStar  float64 `json:"rating_star"`
			RatingCount []int   `json:"rating_count"`
		} `json:"buyer_rating"`
		ChatDisabled bool `json:"chat_disabled"`
		ShowLiveTab  bool `json:"show_live_tab"`
		ResponseTime int  `json:"response_time"`
		Ctime        int  `json:"ctime"`
		Account      struct {
			Username        string  `json:"username"`
			FollowingCount  int     `json:"following_count"`
			HideLikes       int     `json:"hide_likes"`
			EmailVerified   bool    `json:"email_verified"`
			Fbid            string  `json:"fbid"`
			TotalAvgStar    float64 `json:"total_avg_star"`
			PhoneVerified   bool    `json:"phone_verified"`
			FeedAccountInfo struct {
				CanPostFeed bool `json:"can_post_feed"`
				IsKol       bool `json:"is_kol"`
			} `json:"feed_account_info"`
			Portrait string `json:"portrait"`
			IsSeller bool   `json:"is_seller"`
		} `json:"account"`
		ResponseRate               int         `json:"response_rate"`
		DisableMakeOffer           int         `json:"disable_make_offer"`
		Name                       string      `json:"name"`
		RatingStar                 float64     `json:"rating_star"`
		Country                    string      `json:"country"`
		ShowShopeeVerifiedLabel    bool        `json:"show_shopee_verified_label"`
		Cover                      string      `json:"cover"`
		Place                      string      `json:"place"`
		HasFlashSale               bool        `json:"has_flash_sale"`
		LastActiveTime             int         `json:"last_active_time"`
		RealURLIfMatchingCustomURL interface{} `json:"real_url_if_matching_custom_url"`
	} `json:"data"`
	ErrorMsg interface{} `json:"error_msg"`
	Error    int         `json:"error"`
}

type Brand struct {
	Username         string `json:"username"`
	BrandName        string `json:"brand_name"`
	ShopID           int    `json:"shopid"`
	Logo             string `json:"logo"`
	ShopCollectionID int    `json:"shop_collection_id"`
	LogoPc           string `json:"logo_pc"`
	BrandLabel       int    `json:"brand_label"`
}

type BrandResp struct {
	Version string           `json:"version"`
	Data    map[string]Brand `json:"data"`
}

type CategoryResp []struct {
	Main struct {
		DisplayName        string      `json:"display_name"`
		Name               string      `json:"name"`
		Catid              int         `json:"catid"`
		Image              string      `json:"image"`
		ParentCategory     int         `json:"parent_category"`
		IsAdult            int         `json:"is_adult"`
		BlockBuyerPlatform interface{} `json:"block_buyer_platform"`
		SortWeight         int         `json:"sort_weight"`
	} `json:"main"`
	Sub []struct {
		DisplayName        string      `json:"display_name"`
		Name               string      `json:"name"`
		Catid              int         `json:"catid"`
		Image              string      `json:"image"`
		ParentCategory     int         `json:"parent_category"`
		IsAdult            int         `json:"is_adult"`
		BlockBuyerPlatform interface{} `json:"block_buyer_platform"`
		SortWeight         int         `json:"sort_weight"`
		SubSub             []struct {
			Image              string      `json:"image"`
			BlockBuyerPlatform interface{} `json:"block_buyer_platform"`
			DisplayName        string      `json:"display_name"`
			Name               string      `json:"name"`
			Catid              int         `json:"catid"`
		} `json:"sub_sub"`
	} `json:"sub"`
}

type FullSearchResp struct {
	ShowDisclaimer bool `json:"show_disclaimer"`
	QueryRewrite   struct {
		RewriteType        int    `json:"rewrite_type"`
		FEQueryWriteStatus int    `json:"FEQueryWriteStatus"`
		OriKeyword         string `json:"ori_keyword"`
		OriTotalCount      int    `json:"ori_totalCount"`
	} `json:"query_rewrite"`
	Adjust struct {
		Count int `json:"count"`
	} `json:"adjust"`
	Version         string        `json:"version"`
	Algorithm       string        `json:"algorithm"`
	TotalCount      int           `json:"total_count"`
	Error           interface{}   `json:"error"`
	TotalAdsCount   int           `json:"total_ads_count"`
	DisclaimerInfos []interface{} `json:"disclaimer_infos"`
	Nomore          bool          `json:"nomore"`
	PriceAdjust     struct {
		Count int `json:"count"`
	} `json:"price_adjust"`
	JSONData            string `json:"json_data"`
	SuggestionAlgorithm int    `json:"suggestion_algorithm"`
	Items               []struct {
		Itemid                         int64         `json:"itemid"`
		WelcomePackageInfo             interface{}   `json:"welcome_package_info"`
		Liked                          bool          `json:"liked"`
		RecommendationInfo             interface{}   `json:"recommendation_info"`
		BundleDealInfo                 interface{}   `json:"bundle_deal_info"`
		PriceMaxBeforeDiscount         int           `json:"price_max_before_discount"`
		TrackingInfo                   interface{}   `json:"tracking_info"`
		Image                          string        `json:"image"`
		RecommendationAlgorithm        interface{}   `json:"recommendation_algorithm"`
		IsCcInstallmentPaymentEligible bool          `json:"is_cc_installment_payment_eligible"`
		Shopid                         int           `json:"shopid"`
		CanUseWholesale                bool          `json:"can_use_wholesale"`
		GroupBuyInfo                   interface{}   `json:"group_buy_info"`
		ReferenceItemID                string        `json:"reference_item_id"`
		Currency                       string        `json:"currency"`
		RawDiscount                    int           `json:"raw_discount"`
		ShowFreeShipping               bool          `json:"show_free_shipping"`
		VideoInfoList                  []interface{} `json:"video_info_list"`
		AdsKeyword                     string        `json:"ads_keyword"`
		CollectionID                   interface{}   `json:"collection_id"`
		Images                         []string      `json:"images"`
		MatchType                      int           `json:"match_type"`
		PriceBeforeDiscount            int           `json:"price_before_discount"`
		IsCategoryFailed               bool          `json:"is_category_failed"`
		ShowDiscount                   int           `json:"show_discount"`
		CmtCount                       int           `json:"cmt_count"`
		ViewCount                      int           `json:"view_count"`
		DisplayName                    interface{}   `json:"display_name"`
		Catid                          int           `json:"catid"`
		JSONData                       string        `json:"json_data"`
		UpcomingFlashSale              interface{}   `json:"upcoming_flash_sale"`
		IsOfficialShop                 bool          `json:"is_official_shop"`
		Brand                          string        `json:"brand"`
		PriceMin                       int64         `json:"price_min"`
		LikedCount                     int           `json:"liked_count"`
		CanUseBundleDeal               bool          `json:"can_use_bundle_deal"`
		ShowOfficialShopLabel          bool          `json:"show_official_shop_label"`
		CoinEarnLabel                  interface{}   `json:"coin_earn_label"`
		PriceMinBeforeDiscount         int           `json:"price_min_before_discount"`
		CbOption                       int           `json:"cb_option"`
		Sold                           int           `json:"sold"`
		DeductionInfo                  string        `json:"deduction_info"`
		Stock                          int           `json:"stock"`
		Status                         int           `json:"status"`
		PriceMax                       int64         `json:"price_max"`
		AddOnDealInfo                  interface{}   `json:"add_on_deal_info"`
		IsGroupBuyItem                 interface{}   `json:"is_group_buy_item"`
		FlashSale                      interface{}   `json:"flash_sale"`
		Price                          int64         `json:"price"`
		ShopLocation                   string        `json:"shop_location"`
		ItemRating                     struct {
			RatingStar        float64 `json:"rating_star"`
			RatingCount       []int   `json:"rating_count"`
			RcountWithImage   int     `json:"rcount_with_image"`
			RcountWithContext int     `json:"rcount_with_context"`
		} `json:"item_rating"`
		ShowOfficialShopLabelInTitle          bool          `json:"show_official_shop_label_in_title"`
		TierVariations                        []interface{} `json:"tier_variations"`
		IsAdult                               interface{}   `json:"is_adult"`
		Discount                              interface{}   `json:"discount"`
		Flag                                  int           `json:"flag"`
		IsNonCcInstallmentPaymentEligible     bool          `json:"is_non_cc_installment_payment_eligible"`
		HasLowestPriceGuarantee               bool          `json:"has_lowest_price_guarantee"`
		HasGroupBuyStock                      interface{}   `json:"has_group_buy_stock"`
		PreviewInfo                           interface{}   `json:"preview_info"`
		WelcomePackageType                    int           `json:"welcome_package_type"`
		Name                                  string        `json:"name"`
		Distance                              interface{}   `json:"distance"`
		Adsid                                 int           `json:"adsid"`
		Ctime                                 int           `json:"ctime"`
		WholesaleTierList                     []interface{} `json:"wholesale_tier_list"`
		ShowShopeeVerifiedLabel               bool          `json:"show_shopee_verified_label"`
		Campaignid                            int           `json:"campaignid"`
		ShowOfficialShopLabelInNormalPosition interface{}   `json:"show_official_shop_label_in_normal_position"`
		ItemStatus                            string        `json:"item_status"`
		ShopeeVerified                        bool          `json:"shopee_verified"`
		HiddenPriceDisplay                    interface{}   `json:"hidden_price_display"`
		SizeChart                             interface{}   `json:"size_chart"`
		ItemType                              int           `json:"item_type"`
		ShippingIconType                      interface{}   `json:"shipping_icon_type"`
		CampaignStock                         interface{}   `json:"campaign_stock"`
		LabelIds                              []int         `json:"label_ids"`
		ServiceByShopeeFlag                   int           `json:"service_by_shopee_flag"`
		BadgeIconType                         int           `json:"badge_icon_type"`
		HistoricalSold                        int           `json:"historical_sold"`
		TransparentBackgroundImage            string        `json:"transparent_background_image"`
	} `json:"items"`
	ReservedKeyword string        `json:"reserved_keyword"`
	HintKeywords    []interface{} `json:"hint_keywords"`
}

type ShopSearchResp struct {
	Version string `json:"version"`
	Data    struct {
		TotalCount int    `json:"total_count"`
		JSONData   string `json:"json_data"`
		Users      []struct {
			Username       string `json:"username"`
			Followed       bool   `json:"followed"`
			Shopname       string `json:"shopname"`
			FollowingCount int    `json:"following_count"`
			Shopid         int    `json:"shopid"`
			Portrait       string `json:"portrait"`
			JSONData       string `json:"json_data"`
			Customisation  struct {
				ImageURL     interface{} `json:"image_url"`
				DisplayTitle interface{} `json:"display_title"`
				CollectionID interface{} `json:"collection_id"`
			} `json:"customisation"`
			LastLoginTime int `json:"last_login_time"`
			Data          struct {
				TotalCount int           `json:"total_count"`
				Items      []interface{} `json:"items"`
			} `json:"data"`
			IsOfficialShop          bool    `json:"is_official_shop"`
			Score                   int     `json:"score"`
			ShowOfficialShopLabel   bool    `json:"show_official_shop_label"`
			FollowerCount           int     `json:"follower_count"`
			ShopeeVerifiedFlag      int     `json:"shopee_verified_flag"`
			Status                  int     `json:"status"`
			RecallType              int     `json:"recall_type"`
			Nickname                string  `json:"nickname"`
			ResponseTime            int     `json:"response_time"`
			PickupAddressID         int     `json:"pickup_address_id"`
			Country                 string  `json:"country"`
			ShowShopeeVerifiedLabel bool    `json:"show_shopee_verified_label"`
			Userid                  int     `json:"userid"`
			Adsid                   int     `json:"adsid"`
			Products                int     `json:"products"`
			ResponseRate            int     `json:"response_rate"`
			ShopRating              float64 `json:"shop_rating"`
		} `json:"users"`
		Algorithm interface{} `json:"algorithm"`
	} `json:"data"`
	ErrorMsg interface{} `json:"error_msg"`
	Error    int         `json:"error"`
}
