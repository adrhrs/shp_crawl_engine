package main

import (
	"encoding/json"
	"log"
	"strings"
	"time"
)

func (a *App) pingDetail(worker, limit int) (resp PingResp) {

	sellers, _ := a.GetSellerList()
	items := []Item{}
	details := []Detail{}

	for _, s := range sellers {
		products, _ := a.GetSellerProducts(s.ShopID)
		for _, p := range products {
			i := Item{ItemID: p.ItemID, ShopID: p.ShopID}
			items = append(items, i)
		}
	}

	items = items[:limit]

	numJobs := len(items)
	jobs := make(chan Item, numJobs)
	results := make(chan Detail, numJobs)

	t0 := time.Now()

	for w := 1; w <= worker; w++ {
		go a.workerGetProductDetail(w, jobs, results)
	}

	for _, i := range items {
		jobs <- i
	}
	close(jobs)

	failCount := 0

	for a := 1; a <= numJobs; a++ {
		d := <-results
		if d.Item.Itemid == 0 {
			failCount++
		}
		details = append(details, d)
	}

	dur := time.Since(t0)

	resp.TotalRequest = len(details)
	resp.TotalDuration = dur.String()
	resp.TotalWorker = worker

	each := float64(dur/time.Millisecond) / float64(numJobs)

	resp.EachDuration = each
	resp.MinItemID = failCount

	if numJobs > 0 {
		resp.Prove = details[0].Item.Name
	}

	return
}

//CategoryList handle category list
func (a *App) CategoryList() (resp CategoryResp) {

	resp = a.hitCategoryListAPI()
	totProduct := 0

	go func() {

		for _, lv1 := range resp {
			for _, lv2 := range lv1.Sub {
				for _, lv3 := range lv2.SubSub {

					sp := SearchParam{
						MinPrice:   "0",
						Matchid:    lv3.Catid,
						isCategory: true,
					}
					p := Param{
						Data:   sp,
						Offset: 0,
					}

					pr := a.doSearchCrawl(p)

					totProduct += pr.TotalCount

					log.Println(lv3.DisplayName, "\t", pr.TotalCount)

				}
			}
		}

		log.Println("total product", totProduct)

	}()

	return
}

//MasterProductProject usecase to replicate shopee products
func (a *App) MasterProductProject(source, level string) (resp interface{}) {

	if source == "category" {
		go a.MainCategoryBasedCrawlV2()
	} else if source == "shop" {
		go a.MainShopBasedCrawl(level)
	} else if source == "os" {
		oslist := a.doBrandListCrawl()
		for _, v := range oslist.Data {
			log.Println(v.ShopID, v.BrandName)
		}
	} else if source == "prod" {
		go a.MainDetailCrawl()
	}

	resp = "done, check the fucking log"
	return
}

func (a *App) MainShopBasedCrawl(level string) {

	t0 := time.Now()
	workers := 20

	shopIDs := []int{}

	if level == "os" {
		oslist := a.doBrandListCrawl()
		for _, v := range oslist.Data {
			shopIDs = append(shopIDs, v.ShopID)
		}
	} else if level == "shop" {
		shopIDs, _ = a.GetDistinctShopID()
		if len(shopIDs) > 1000 {
			shopIDs = shopIDs[:1000]
		}
	}

	log.Println("crawling by shop", len(shopIDs), level)

	SPs := []SearchParam{}
	items := []Item{}

	mapItems := make(map[int64]Item)
	keys := []ProductMasterKey{}

	for _, s := range shopIDs {
		sp := SearchParam{
			MinPrice:   "0",
			WantedItem: 8000,
			Matchid:    s,
			SortBy:     "sales",
		}
		SPs = append(SPs, sp)
	}

	numJobs := len(SPs)
	jobs := make(chan SearchParam, numJobs)
	results := make(chan []Item, numJobs)

	for w := 1; w <= workers; w++ {
		go a.workerSearch(w, jobs, results, false)
	}

	for _, j := range SPs {
		jobs <- j
	}
	close(jobs)

	for a := 1; a <= numJobs; a++ {
		items = append(items, <-results...)
	}

	for _, i := range items {
		mapItems[i.ItemID] = i
	}

	for _, v := range mapItems {
		key := ProductMasterKey{
			ItemID:        v.ItemID,
			ShopID:        v.ShopID,
			Lv3CategoryID: v.ShopID,
			SourceCrawl:   "shop",
			UpdateTime:    getTimeGMT(),
		}
		keys = append(keys, key)

		if len(keys) == 1000 {
			err := a.InsertProductKeyBulk(keys)
			if err != nil {
				log.Println(err)
			}
			keys = []ProductMasterKey{}
		}
	}

	if len(keys) > 0 {
		err := a.InsertProductKeyBulk(keys)
		if err != nil {
			log.Println(err)
		}
	}

	log.Println("done by shop", len(SPs), len(items), len(mapItems), time.Since(t0))

}

//MainCategoryBasedCrawl handle main cat function
func (a *App) MainCategoryBasedCrawl() {

	t0 := time.Now()
	items := []Item{}
	keys := []ProductMasterKey{}

	mapItems := make(map[int64]Item)

	cat := a.hitCategoryListAPI()
	doneCrawl, _ := a.GetAllProductKeys()
	SPs, _ := mapCategoryID(cat, doneCrawl, 50)

	numJobs := len(SPs)
	jobs := make(chan SearchParam, numJobs)
	results := make(chan []Item, numJobs)

	for w := 1; w <= 10; w++ {
		go a.workerSearch(w, jobs, results, true)
	}

	for _, j := range SPs {
		jobs <- j
	}
	close(jobs)

	for a := 1; a <= numJobs; a++ {
		items = append(items, <-results...)
	}

	for _, i := range items {
		mapItems[i.ItemID] = i
	}

	for _, v := range mapItems {
		key := ProductMasterKey{
			ItemID:        v.ItemID,
			ShopID:        v.ShopID,
			Lv3CategoryID: v.Catid,
			SourceCrawl:   "category",
			UpdateTime:    getTimeGMT(),
		}
		keys = append(keys, key)

		if len(keys) == 1000 {
			err := a.InsertProductKeyBulk(keys)
			if err != nil {
				log.Println(err)
			}
			keys = []ProductMasterKey{}
		}
	}

	if len(keys) > 0 {
		err := a.InsertProductKeyBulk(keys)
		if err != nil {
			log.Println(err)
		}
	}

	log.Println(numJobs, len(items), len(mapItems), time.Since(t0))

}

func (a *App) workerSearch(id int, jobs <-chan SearchParam, results chan<- []Item, useRelevance bool) {
	for j := range jobs {

		allItem := []Item{}
		returnedItem := []Item{}

		if useRelevance {
			relevanceItems, _ := a.paginateProductCrawl(j)
			allItem = append(allItem, relevanceItems...)
		}

		j.SortBy = "sales"
		topSalesItems, _ := a.paginateProductCrawl(j)
		allItem = append(allItem, topSalesItems...)

		for _, i := range allItem {
			i.Catid = j.Matchid
			returnedItem = append(returnedItem, i)
		}

		results <- returnedItem
	}
}

func mapCategoryID(cat CategoryResp, doneCrawl []ProductMasterKey, limit int) (SPs []SearchParam, progress float64) {

	dc := make(map[int]int)
	for _, d := range doneCrawl {
		dc[d.Lv3CategoryID]++
	}
	var totalCat float64
	for _, lv1 := range cat {
		for _, lv2 := range lv1.Sub {
			for _, lv3 := range lv2.SubSub {
				if _, ok := dc[lv3.Catid]; !ok {
					sp := SearchParam{
						MinPrice:   "0",
						WantedItem: 8000,
						Matchid:    lv3.Catid,
						isCategory: true,
					}
					SPs = append(SPs, sp)
				}
				totalCat++
			}
		}
	}

	progress = (float64(len(dc)) / totalCat) * 100

	if len(SPs) >= limit {
		SPs = SPs[:limit]
	}

	// log.Println(len(SPs), "done crawl by cat", len(dc), "progress", progress)

	return
}

func (a *App) MainDetailCrawl() {

	t0 := time.Now()
	unc, _ := a.GetUnDetailedProductKeys()

	log.Println("getting detail for", len(unc), "query time", time.Since(t0))

	tempUnc := []ProductMasterKey{}
	items := []Item{}
	products := []EsProduct{}

	var insPG, insES int

	t3 := time.Now()

	for i, v := range unc {

		upd := &unc[i]
		upd.DetailStatus = 1

		tempUnc = append(tempUnc, unc[i])
		if len(tempUnc) == 1000 {

			err := a.UpdateProductKeyBulk(unc)
			if err != nil {
				log.Println(err)
			}
			insPG++
			tempUnc = []ProductMasterKey{}
		}

		item := Item{
			ItemID: v.ItemID,
			ShopID: v.ShopID,
		}
		items = append(items, item)
	}

	log.Println("update db data", len(items), "query time", time.Since(t3))

	t1 := time.Now()
	details := a.poolGetProductDetail(items)
	t2 := time.Now()

	for _, d := range details {
		p := mapDetailsToEsProduct(d)
		products = append(products, p)
		if len(products) == 200 {
			err := a.EsInsertProductBulk(products)
			if err != nil {
				log.Println(err)
			}
			insES++
			products = []EsProduct{}
		}
	}

	log.Println("update to es", len(details), "query time", time.Since(t2))

	if len(tempUnc) > 0 {
		err := a.UpdateProductKeyBulk(unc)
		if err != nil {
			log.Println(err)
		}
		insPG++
	}

	if len(products) > 0 {
		err := a.EsInsertProductBulk(products)
		if err != nil {
			log.Println(err)
		}
		insES++
	}

	log.Println("done", insPG, insES, t2.Sub(t1), time.Since(t0))
}

func mapDetailsToEsProduct(d Detail) (e EsProduct) {

	i, err := json.Marshal(d.Item)
	if err != nil {
		log.Println(err)
	}
	err = json.Unmarshal(i, &e)
	if err != nil {
		log.Println(err)
	}

	e.Description = strings.Replace(e.Description, "\n", " ", -1)
	e.Description = strings.Replace(e.Description, "  ", " ", -1)

	return
}

/// v2

func (a *App) MainCategoryBasedCrawlV2() {

	t0 := time.Now()

	keys := []ProductMasterKey{}
	eps := []EsProduct{}
	mapItems := make(map[int64]Item)
	var insES, insPG, totProd int

	t := time.Now()
	cat := a.hitCategoryListAPI()
	doneCrawl, _ := a.GetAllProductKeys()
	SPs, prog := mapCategoryID(cat, doneCrawl, 2)
	log.Println("preparing category, curr progress", prog, time.Since(t))
	log.Println("Categories", SPs)

	// loop each category, currently 1
	for _, s := range SPs {

		// loop each page by relevance and sales
		t = time.Now()
		allIds := []Item{}

		ids, _ := a.paginateProductCrawl(s)
		allIds = append(allIds, ids...)

		s.SortBy = "sales"
		ids, _ = a.paginateProductCrawl(s)
		allIds = append(allIds, ids...)

		for _, i := range allIds {
			mapItems[i.ItemID] = i
		}

		distItems := []Item{}

		for _, v := range mapItems {
			distItems = append(distItems, v)
		}

		log.Println("hitting search got ", len(ids), len(distItems), "begin hitting detail...", time.Since(t))

		// pool get details
		t = time.Now()
		products := a.poolGetProductDetail(distItems)
		log.Println("hitting detail got", len(products), "begin storing data...", time.Since(t))

		// insert DB and ES
		t = time.Now()
		for _, p := range products {

			k := ProductMasterKey{
				ItemID:        p.Item.Itemid,
				ShopID:        p.Item.Shopid,
				Lv3CategoryID: s.Matchid,
				DetailStatus:  1,
				SourceCrawl:   "category",
			}
			keys = append(keys, k)

			ep := mapDetailsToEsProduct(p)
			eps = append(eps, ep)

			if len(keys) == 1000 {
				log.Println("inserting", insPG)
				err := a.InsertProductKeyBulk(keys)
				if err != nil {
					log.Println(err)
				}
				keys = []ProductMasterKey{}
				insPG++
			}

			if len(eps) == 500 {
				a.EsInsertProductBulk(eps)
				eps = []EsProduct{}
				insES++
			}

		}

		if len(keys) > 0 {
			a.InsertProductKeyBulk(keys)
			insPG++
		}

		if len(eps) > 0 {
			a.EsInsertProductBulk(eps)
			insES++
		}

		totProd += len(products)

		log.Println("storing data", time.Since(t))
	}

	log.Println("------ finish all ------", len(SPs), totProd, insPG, insES, time.Since(t0))

}
