package main

import (
	"log"
	"time"
)

func (a *App) RegisterCronInfo() {

	t0 := time.Now()

	sellers, err := a.GetSellerList()
	if err != nil {
		return
	}

	for _, s := range sellers {

		t1 := time.Now()

		newSellerResp := a.doSellerCrawl(s.Username)
		newSellerData := mapSellerInfoToSeller(newSellerResp)

		newSellerData.ID = s.ID
		err = a.UpdateSellerInfo(newSellerData)
		if err != nil {
			continue
		}

		hist, err := a.MarkSellerHistory(newSellerData)
		if err != nil {
			continue
		}

		err = a.DeleteProductByShopID(s.ShopID)
		if err != nil {
			continue
		}

		newProducts, err := a.SearchSellerProduct(s)
		if err != nil {
			continue
		}

		log.Printf("cron seller info %s , found new products %d, durration %s ", hist.Username, len(newProducts), time.Since(t1))

	}

	log.Printf("cron finished, duration all %s", time.Since(t0))

	return
}

func (a *App) RegisterCronMonitorShpMall() {

	t0 := time.Now()

	a.InitiateMonitorShpMall()

	log.Printf("cron finished, duration all %s", time.Since(t0))

	return
}

func (a *App) RegisterCronCategoryBasedCrawl() {

	t0 := time.Now()

	a.MainCategoryBasedCrawlV2()

	log.Printf("cron finished, duration all %s", time.Since(t0))

	return
}

func (a *App) RegisterCronShopBasedCrawl() {

	t0 := time.Now()

	a.MainShopBasedCrawl("shop")

	log.Printf("cron finished, duration all %s", time.Since(t0))

	return
}

func (a *App) RegisterCronDetailCrawl() {

	t0 := time.Now()

	a.MainDetailCrawl()

	log.Printf("cron finished, duration all %s", time.Since(t0))

	return
}

func (a *App) RegisterCronPSCronSearch() {

	t0 := time.Now()

	a.PSCronSearch()

	log.Printf("cron finished, duration all %s", time.Since(t0))

	return
}
