package main

import (
	"log"

	pg "github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

// PRODUCT

func (a *App) InsertProduct(data Product) (err error) {

	data.UpdateTime = getTimeGMT()

	err = a.DB.Insert(&data)
	if err != nil {
		pgErr, ok := err.(pg.Error)
		if ok && pgErr.IntegrityViolation() {
			err = nil
		} else {
			log.Println(err)
		}
	}
	return
}

func (a *App) GetSellerProducts(shopID int) (products []Product, err error) {

	filter := func(q *orm.Query) (*orm.Query, error) {
		q = q.Where("shop_id = ?", shopID)
		return q, nil
	}

	err = a.DB.Model(&products).Apply(filter).Select()
	if err != nil {
		return
	}

	return
}

func (a *App) UpdateProduct(data Product) (err error) {

	data.UpdateTime = getTimeGMT()

	err = a.DB.Update(&data)
	if err != nil {
		log.Println(err)
		return
	}

	return
}

func (a *App) DeleteProductByShopID(shopID int) (err error) {

	products := []Product{}

	filter := func(q *orm.Query) (*orm.Query, error) {
		q = q.Where("shop_id = ?", shopID)
		return q, nil
	}

	err = a.DB.Model(&products).Apply(filter).Select()
	if err != nil {
		log.Println(err)
		return
	}

	if len(products) > 0 {
		_, err = a.DB.Model(&products).Delete()
		if err != nil {
			log.Println(err)
			return
		}
	}

	return
}

// PRODUCT HIST

func (a *App) InsertProductHistory(data ProductHistory) (err error) {

	data.UpdateTime = getTimeSpan()

	err = a.DB.Insert(&data)
	if err != nil {
		pgErr, ok := err.(pg.Error)
		if ok && pgErr.IntegrityViolation() {
			err = nil
		} else {
			log.Println(err)
		}
	}
	return
}

func (a *App) InsertProductHistoryV2(data ProductHistoryV2) (err error) {

	err = a.DB.Insert(&data)
	if err != nil {
		pgErr, ok := err.(pg.Error)
		if ok && pgErr.IntegrityViolation() {
			err = nil
		} else {
			log.Println(err)
		}
	}
	return
}

func (a *App) InsertProductHistoryBulkV2(data []ProductHistoryV2) (err error) {

	err = a.DB.Insert(&data)
	if err != nil {
		pgErr, ok := err.(pg.Error)
		if ok && pgErr.IntegrityViolation() {
			log.Println(err)
			err = nil
		} else {
			log.Println(err)
		}
	}
	return
}
