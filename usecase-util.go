package main

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"
)

var (
	IMAGE_URL = "https://cf.shopee.co.id/file/"
)

func convertTime(t time.Duration) (f string) {

	d := time.Millisecond
	f = (t.Truncate(d)).String()

	return
}

func findMax(v1, v2 int) int {
	if v1 < v2 {
		return v1
	}

	return v2
}

func ts(v int) string {
	return strconv.Itoa(v)
}

func mapSellerInfoToSeller(i SellerInfo) (o Seller) {

	d := i.Data
	o.UserID = d.Userid
	o.ShopID = d.Shopid
	o.Name = d.Name
	o.Portrait = IMAGE_URL + d.Account.Portrait
	o.Description = d.Description
	o.Place = d.Place
	o.Username = d.Account.Username
	o.ItemCount = d.ItemCount
	o.CancellationRate = d.CancellationRate
	o.FollowerCount = d.FollowerCount
	o.ResponseTime = d.ResponseTime
	o.RatingGood = d.RatingGood
	o.ResponseRate = d.ResponseRate
	o.PreparationTime = d.PreparationTime
	o.Ctime = d.Ctime
	o.RatingStar = d.RatingStar
	o.LastActiveTime = d.LastActiveTime

	if d.IsOfficialShop {
		o.IsOfficialShop = true
	} else {
		o.IsOfficialShop = false
	}

	return
}

func mapSellerToSellerHistory(d Seller) (o SellerHistory) {

	o.ShopID = d.ShopID
	o.Username = d.Username
	o.ItemCount = d.ItemCount
	o.FollowerCount = d.FollowerCount
	o.ResponseTime = d.ResponseTime
	o.RatingGood = d.RatingGood
	o.ResponseRate = d.ResponseRate
	o.PreparationTime = d.PreparationTime
	o.RatingStar = d.RatingStar

	return

}

func MapDetailToProduct(d Detail) (p Product) {

	i := d.Item

	p.ItemID = i.Itemid
	p.ShopID = i.Shopid
	p.Name = i.Name
	p.Brand = i.Brand
	p.Images = strings.Join(i.Images, ",")
	p.Description = i.Description

	p.Stock = i.Stock
	p.Price = i.Price
	p.HistoricalSold = i.HistoricalSold
	p.RatingStar = i.ItemRating.RatingStar
	p.RatingCount = i.ItemRating.RatingCount[0]
	p.LikedCount = i.LikedCount

	tempArr := []string{}
	tempArrID := []string{}

	for _, c := range i.Categories {
		tempArr = append(tempArr, c.DisplayName)
		tempArrID = append(tempArrID, strconv.Itoa(c.Catid))
	}

	p.CategoryID = strings.Join(tempArrID, ",")
	p.CategoryName = strings.Join(tempArr, ",")

	p.ViewCount = d.Item.ViewCount

	return
}

func MapProductToProductHist(i Product) (p ProductHistory) {

	p.ItemID = i.ItemID
	p.ShopID = i.ShopID
	p.Stock = i.Stock
	p.Price = i.Price
	p.HistoricalSold = i.HistoricalSold
	p.RatingStar = i.RatingStar
	p.RatingCount = i.RatingCount
	p.LikedCount = i.LikedCount
	p.ViewCount = i.ViewCount

	return
}

func MapProductToProductHistV2(i Item) (p ProductHistoryV2) {

	p.ItemID = i.ItemID
	p.ShopID = i.ShopID
	p.Stock = i.Stock
	p.Price = i.Price / 100000
	p.HistoricalSold = i.HistoricalSold
	p.RatingStar = i.ItemRating.RatingStar
	p.RatingCount = i.ItemRating.RatingCount[0]
	p.LikedCount = i.LikedCount
	p.ViewCount = i.ViewCount

	p.UpdateTime = getTimeSpan()

	return
}

func splitProductHistoryV2(buf []ProductHistoryV2, lim int) [][]ProductHistoryV2 {
	var chunk []ProductHistoryV2
	chunks := make([][]ProductHistoryV2, 0, len(buf)/lim+1)
	for len(buf) >= lim {
		chunk, buf = buf[:lim], buf[lim:]
		chunks = append(chunks, chunk)
	}
	if len(buf) > 0 {
		chunks = append(chunks, buf[:len(buf)])
	}
	return chunks
}

// construct URL shopee PDP using product name, shopID and itemID
func constructURL(productName string, itemID int64, shopID int) (URL string) {
	// define regex
	regexSpace := regexp.MustCompile(`\s+`)

	// trim multiple spaces and special char
	productName = regexSpace.ReplaceAllString(productName, " ")

	// define replacer
	replacer := strings.NewReplacer(" - ", "-", " -", "-", "- ", "-", " ", "-")
	productName = replacer.Replace(productName)

	// construct URL
	URL = fmt.Sprintf("%s-i.%d.%d", productName, shopID, itemID)

	URL = "https://shopee.co.id/" + URL

	return
}
