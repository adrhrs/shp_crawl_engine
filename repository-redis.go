package main

import (
	"encoding/json"
	"time"
)

func (a *App) SetRedis(key string, value interface{}) error {
	p, err := json.Marshal(value)
	if err != nil {
		return err
	}
	_, err = a.Redis.Set(key, p, 5*time.Hour).Result()
	return err
}

func (a *App) GetRedis(key string) (value string, err error) {
	value, err = a.Redis.Get(key).Result()
	if err != nil {
		return
	}
	return
}

func (a *App) ScanRedis(key string) (keys [][]string, err error) {
	limit := int64(1000)
	var cursor uint64
	var batchedKeys []string
	var errScan error
	for {
		batchedKeys, cursor, errScan = a.Redis.Scan(cursor, key, limit).Result()
		if errScan != nil {
			err = errScan
			return
		}
		keys = append(keys, batchedKeys)
		if cursor == 0 {
			break
		}
	}
	return
}
