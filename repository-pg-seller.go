package main

import (
	"log"

	pg "github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

func (a *App) InsertSellerInfo(data Seller) (err error) {

	data.UpdateTime = getTimeGMT()

	err = a.DB.Insert(&data)
	if err != nil {
		log.Println(err)
		return
	}
	return
}

func (a *App) GetSellerInfo(username string) (seller Seller, err error) {

	filter := func(q *orm.Query) (*orm.Query, error) {
		q = q.Where("username = ?", username)
		return q, nil
	}

	err = a.DB.Model(&seller).Apply(filter).First()
	if err != nil {
		return
	}

	return
}

func (a *App) GetSellerList() (seller []Seller, err error) {

	filter := func(q *orm.Query) (*orm.Query, error) {
		q = q.Where("shop_id > ?", 0)
		return q, nil
	}

	err = a.DB.Model(&seller).Apply(filter).Select()
	if err != nil {
		return
	}

	return
}

func (a *App) GetSellerListV2() (data []GetShopListWithStatistic, err error) {

	_, err = a.DB.Query(&data, getShopListWithStatistic)
	if err != nil {
		log.Println(err)
		return
	}

	return
}

func (a *App) UpdateSellerInfo(data Seller) (err error) {

	data.UpdateTime = getTimeGMT()

	err = a.DB.Update(&data)
	if err != nil {
		log.Println(err)
		return
	}

	return
}

// SELLER HIST

func (a *App) InsertSellerHistory(data SellerHistory) (err error) {

	data.UpdateTime = getTimeSpan()

	err = a.DB.Insert(&data)
	if err != nil {
		pgErr, ok := err.(pg.Error)
		if ok && pgErr.IntegrityViolation() {
			err = nil
		} else {
			log.Println(err)
		}
	}
	return
}

func (a *App) GetSellerHistoryV2(shopID int, start, finish string) (data []GetSellerHistory, err error) {

	_, err = a.DB.Query(&data, getSellerHistory, shopID, start, finish, start, finish)
	if err != nil {
		log.Println(err)
		return
	}

	return
}

func (a *App) GetSellerHistoryV3(shopID int, start, finish string) (data []GetSellerHistoryV3, err error) {

	_, err = a.DB.Query(&data, getSellerHistoryV3, shopID, start, finish)
	if err != nil {
		log.Println(err)
		return
	}

	return
}

func (a *App) GetSellerRevenue(shopID int, start, finish string) (data []GetSellerRevenue, err error) {

	_, err = a.DB.Query(&data, getSellerRevenue, shopID, start, finish)
	if err != nil {
		log.Println(err)
		return
	}

	return
}

// SALES CHART

func (a *App) GetSellerSales(shopID int, start, finish string) (data []GetSalesAggregate, err error) {

	_, err = a.DB.Query(&data, getSalesAggregate, shopID, start, finish)
	if err != nil {
		log.Println(err)
		return
	}

	return
}

// HEADER

func (a *App) GetSellerSummary(shopID int) (data []GetSummarySellerProduct, err error) {

	_, err = a.DB.Query(&data, getSummarySellerProduct, shopID)
	if err != nil {
		log.Println(err)
		return
	}

	return
}
