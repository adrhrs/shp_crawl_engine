package main

import (
	"encoding/json"
	"net/http"
	"strconv"
)

func IndexHandler(w http.ResponseWriter, r *http.Request) {

	res := Response{}

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	msg := Index()
	res.Data = msg

	return

}

func (a *App) SearchHandler(w http.ResponseWriter, r *http.Request) {

	res := Response{}

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	keyword := r.FormValue("keyword")
	if keyword == "" {
		res.Message = "keyword is required"
		return
	}

	minPrice := r.FormValue("min_price")
	if minPrice == "" {
		res.Message = "min_price is required"
		return
	}

	wantedItem, err := strconv.ParseFloat(r.FormValue("wanted_item"), 64)
	if err != nil {
		res.Message = err.Error()
		return
	}

	param := SearchParam{
		Keyword:    keyword,
		MinPrice:   minPrice,
		WantedItem: wantedItem,
	}

	msg, items := a.Search(param)
	res.Message = msg
	res.Data = items

	return

}

func (a *App) SellerInfoHandler(w http.ResponseWriter, r *http.Request) {

	res := Response{}

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	username := r.FormValue("username")
	if username == "" {

		sellers, msg, err := a.SellerList()
		if err != nil {
			res.Message = err.Error()
			return
		}

		res.Message = msg
		res.Data = sellers

	} else {

		seller, msg, err := a.SellerInfo(username)
		if err != nil {
			res.Message = err.Error()
			return
		}
		res.Message = msg
		res.Data = seller

	}

	return

}

func (a *App) SellerProductsHandler(w http.ResponseWriter, r *http.Request) {

	res := Response{}

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	username := r.FormValue("username")
	if username == "" {
		res.Message = "username is required"
		return
	}

	seller, msg, err := a.SellerProducts(username)
	if err != nil {
		res.Message = err.Error()
		return
	}

	res.Message = msg
	res.Data = seller

	return

}

func (a *App) SellerChartHandler(w http.ResponseWriter, r *http.Request) {

	res := Response{}

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	username := r.FormValue("username")
	if username == "" {
		res.Message = "username is required"
		return
	}

	st := r.FormValue("date_start")
	if st == "" {
		res.Message = "date start is required"
		return
	}

	fn := r.FormValue("date_finish")
	if fn == "" {
		res.Message = "date finish is required"
		return
	}

	chart, msg, err := a.SalesChart(username, st, fn)
	if err != nil {
		res.Message = err.Error()
		return
	}

	res.Message = msg
	res.Data = chart

	return

}

func (a *App) SellerHeaderHandler(w http.ResponseWriter, r *http.Request) {

	res := Response{}

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	username := r.FormValue("username")
	if username == "" {
		res.Message = "username is required"
		return
	}

	data, msg, err := a.SellerHeader(username)
	if err != nil {
		res.Message = err.Error()
		return
	}

	res.Message = msg
	res.Data = data

	return

}

func (a *App) SellerListHandler(w http.ResponseWriter, r *http.Request) {

	res := Response{}

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	data, msg, err := a.SellerList()
	if err != nil {
		res.Message = err.Error()
		return
	}

	res.Message = msg
	res.Data = data

	return

}

func (a *App) SellerListHandlerV2(w http.ResponseWriter, r *http.Request) {

	res := Response{}

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	data, msg, err := a.SellerListV2()
	if err != nil {
		res.Message = err.Error()
		return
	}

	res.Message = msg
	res.Data = data

	return

}

func (a *App) SellerHistoryHandler(w http.ResponseWriter, r *http.Request) {
	res := Response{}

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	username := r.FormValue("username")
	if username == "" {
		res.Message = "username is required"
		return
	}

	start := r.FormValue("date_start")
	if start == "" {
		res.Message = "date start is required"
		return
	}

	finish := r.FormValue("date_finish")
	if finish == "" {
		res.Message = "date finish is required"
		return
	}

	historyType := r.FormValue("history_type")
	if finish == "" {
		res.Message = "history type is required"
		return
	}

	history, msg, err := a.SellerHistory(username, start, finish, historyType)
	if err != nil {
		res.Message = err.Error()
		return
	}

	res.Message = msg
	res.Data = history

	return

}

func (a *App) SellerRevenueHandler(w http.ResponseWriter, r *http.Request) {
	res := Response{}

	defer func() {

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(res)

	}()

	username := r.FormValue("username")
	if username == "" {
		res.Message = "username is required"
		return
	}

	start := r.FormValue("date_start")
	if start == "" {
		res.Message = "date start is required"
		return
	}

	finish := r.FormValue("date_finish")
	if finish == "" {
		res.Message = "date finish is required"
		return
	}

	history, msg, err := a.SellerRevenue(username, start, finish)
	if err != nil {
		res.Message = err.Error()
		return
	}

	res.Message = msg
	res.Data = history

	return

}
