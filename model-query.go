package main

import "time"

type GetSalesAggregate struct {
	ItemID         int64     `db:"item_id"`
	HistoricalSold int       `db:"historical_sold"`
	LikedCount     int       `db:"liked_count"`
	RatingCount    int       `db:"rating_count"`
	ViewCount      int       `db:"view_count"`
	Price          int       `db:"price"`
	UpdateTime     time.Time `db:"update_time"`
}

type GetSummarySellerProduct struct {
	ShopID         int64     `db:"shop_id" json:"shop_id"`
	HistoricalSold int       `db:"historical_sold" json:"historical_sold"`
	Revenue        int       `db:"revenue" json:"revenue"`
	LikedCount     int       `db:"liked_count" json:"liked_count"`
	RatingCount    int       `db:"rating_count" json:"rating_count"`
	ViewCount      int       `db:"view_count" json:"view_count"`
	ProductCount   int       `db:"product_count" json:"product_count"`
	UpdateTime     time.Time `db:"update_time" json:"update_time"`
}

type GetShopListWithStatistic struct {
	ShopID             int64     `db:"shop_id" json:"shop_id"`
	Name               string    `db:"name" json:"name"`
	Username           string    `db:"username" json:"username"`
	Revenue            int       `db:"revenue" json:"revenue"`
	RatingCount        int       `db:"rating_count" json:"rating_count"`
	FollowerCount      int       `db:"follower_count" json:"follower_count"`
	ProductCount       int       `db:"product_count" json:"product_count"`
	DataCount          int       `db:"data_count" json:"data_count"`
	FirstDate          time.Time `db:"first_date" json:"first_date"`
	LastDate           time.Time `db:"last_date" json:"last_date"`
	FirstDateFormatted string    `json:"first_date_formatted"`
}

type GetSellerHistory struct {
	ShopID      int64     `db:"shop_id" json:"shop_id"`
	ActiveItem  int       `db:"active_item" json:"active_item"`
	PriceMin    int       `db:"price_min" json:"price_min"`
	PriceMax    int       `db:"price_max" json:"price_max"`
	ViewCount   int       `db:"view_count" json:"view_count"`
	SoldCount   int       `db:"sold_count" json:"sold_count"`
	ReviewCount int       `db:"review_count" json:"review_count"`
	RatingAvg   float64   `db:"rating_avg" json:"rating_avg"`
	UpdateTime  time.Time `db:"update_time" json:"update_time"`
}

type GetSellerRevenue struct {
	ItemID      int64     `db:"item_id" json:"item_id"`
	ShopID      int       `db:"shop_id" json:"shop_id"`
	Price       int       `db:"price" json:"price"`
	SoldCount   int       `db:"sold_count" json:"sold_count"`
	ReviewCount int       `db:"review_count" json:"review_count"`
	ViewCount   int       `db:"view_count" json:"view_count"`
	Tgl         time.Time `db:"tgl" json:"tgl"`
}

type GetSellerHistoryV3 struct {
	ItemID      int64     `db:"item_id" json:"item_id"`
	UpdateTime  time.Time `db:"update_time" json:"update_time"`
	Price       int       `db:"price" json:"price"`
	SoldCount   int       `db:"sold_count" json:"sold_count"`
	ViewCount   int       `db:"view_count" json:"view_count"`
	ReviewCount int       `db:"review_count" json:"review_count"`
	RatingAvg   float64   `db:"rating_avg" json:"rating_avg"`
}
