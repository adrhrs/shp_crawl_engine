package main

import (
	"fmt"
	"math"
	"sort"
	"time"

	"github.com/montanaflynn/stats"
)

func (a *App) Search(p SearchParam) (msg string, resp SearchResponse) {

	t1 := time.Now()

	items, productCount := a.paginateProductCrawl(p)

	t2 := time.Now()

	resp.Products = a.poolGetProductDetail(items)

	t3 := time.Now()

	resp.Categories = handleCategory(resp.Products)

	resp.Analytics = analyseProduct(resp.Products)
	resp.Analytics.AllProductCount = int(productCount)

	resp.Perfomance.SearchAPI = convertTime(t2.Sub(t1))
	resp.Perfomance.ProductAPI = convertTime(t3.Sub(t2))
	resp.Perfomance.AllProcess = convertTime(t3.Sub(t1))

	msg = fmt.Sprintf("success getting product with detail (%d)", len(items))

	return
}

func (a *App) paginateProductCrawl(sp SearchParam) (items []Item, productCount float64) {

	totals := []float64{}
	lastOffset := 0

	for {

		p := Param{}
		p.Data = sp
		p.Offset = lastOffset

		resp := a.doSearchCrawl(p)

		if len(resp.Items) == 0 {
			break
		}

		totals = append(totals, float64(resp.TotalCount))
		items = append(items, resp.Items...)

		if float64(len(items)) >= sp.WantedItem {
			break
		}

		lastOffset += len(resp.Items)

		// log.Println(len(resp.Items), len(items))

	}

	if len(items) > int(sp.WantedItem) {
		items = items[0:int(sp.WantedItem)]
	}

	productCount, _ = stats.Max(totals)

	for id := range items {
		items[id].Order = id + 1
	}

	return
}

func (a *App) poolGetProductDetail(items []Item) (details []Detail) {

	numJobs := len(items)
	jobs := make(chan Item, numJobs)
	results := make(chan Detail, numJobs)

	for w := 1; w <= DEFAULT_WORKER; w++ {
		go a.workerGetProductDetail(w, jobs, results)
	}

	for _, j := range items {
		jobs <- j
	}

	for a := 1; a <= numJobs; a++ {
		details = append(details, <-results)
	}

	close(jobs)

	return
}

func (a *App) workerGetProductDetail(id int, jobs <-chan Item, results chan<- Detail) {

	for j := range jobs {
		res := a.doDetailCrawl(j)
		res = handlePrice(res)
		results <- res
	}

}

func handleCategory(details []Detail) (category Category) {

	pCount := make(map[string]int)
	sCount := make(map[string]int)

	for _, d := range details {
		pCount[d.Item.Categories[2].DisplayName]++
		sCount[d.Item.Categories[2].DisplayName] += d.Item.HistoricalSold
	}

	for k, v := range pCount {
		prc := (float64(v) / float64(len(details))) * 100
		prc = (math.Round(prc*100) / 100)

		cat := CategoryData{
			Name:         k,
			ProductCount: v,
			SoldCount:    sCount[k],
			Percentage:   prc,
		}

		if prc < 10 {
			cat.Color = "gradient-danger"
		} else if prc < 50 {
			cat.Color = "gradient-warning"
		} else {
			cat.Color = "gradient-success"
		}

		category.Data = append(category.Data, cat)
	}

	sort.Slice(category.Data, func(i, j int) bool {
		return category.Data[i].ProductCount > category.Data[j].ProductCount
	})

	total := len(category.Data)
	category.CategoryCount = total

	if total > 10 {
		category.Data = category.Data[0:9]
		others := CategoryData{}
		for _, o := range category.Data[9:total] {
			others.Percentage += o.Percentage
			others.ProductCount += o.ProductCount
			others.SoldCount += o.SoldCount
			others.Name = "Others"
		}
		category.Data = append(category.Data, others)
	}

	if total > 0 {
		category.KeywordScore = category.Data[0].Percentage
	}

	return
}

func handlePrice(old Detail) (new Detail) {

	new = old

	new.Item.Price = old.Item.Price / 100000
	new.Item.PriceBeforeDiscount = old.Item.PriceBeforeDiscount / 100000
	new.Item.PriceMinBeforeDiscount = old.Item.PriceMinBeforeDiscount / 100000
	new.Item.PriceMax = old.Item.PriceMax / 100000
	new.Item.PriceMin = old.Item.PriceMin / 100000

	return
}

func analyseProduct(details []Detail) (analytics Analytics) {

	prices := []float64{}
	shops := make(map[int]int)

	for _, d := range details {
		prices = append(prices, d.Item.Price)
		shops[d.Item.Shopid]++
	}

	analytics.ShopCount = len(shops)

	analytics = handleStatistics(analytics, prices)
	analytics = handleLineChart(analytics, prices, details)

	labels := []int{}
	values := []int{}

	for i := 0; i < 6; i++ {
		count := 0
		for _, d := range details {
			r := int(math.Round(d.Item.ItemRating.RatingStar))
			if r == i {
				count++
			}
		}
		labels = append(labels, i)
		values = append(values, count)

	}

	analytics.PriceChart.Bar.Labels = labels
	analytics.PriceChart.Bar.ProductCount = values

	return
}

func handleStatistics(initial Analytics, prices []float64) (analytics Analytics) {

	analytics = initial

	sort.Sort(sort.Float64Slice(prices))

	priceMean, _ := stats.Mean(prices)
	priceMedian, _ := stats.Median(prices)
	priceMax, _ := stats.Max(prices)
	priceMin, _ := stats.Min(prices)
	priceStdev, _ := stats.StdDevP(prices)

	analytics.Prices = prices
	analytics.PriceMean = int(priceMean)
	analytics.PriceMedian = int(priceMedian)
	analytics.PriceMax = int(priceMax)
	analytics.PriceMin = int(priceMin)
	analytics.PriceStdev = math.Round((priceStdev/priceMean)*10000) / 100
	return
}

func handleLineChart(initial Analytics, prices []float64, details []Detail) (analytics Analytics) {

	analytics = initial

	labels := []int{}

	priceVal := []int{}
	salesVal := []int{}
	likeVal := []int{}

	var i float64

	labels = append(labels, 0)
	priceVal = append(priceVal, 0)
	salesVal = append(salesVal, 0)
	likeVal = append(likeVal, 0)

	for i = 0; i < 10; i++ {

		mi, _ := stats.Percentile(prices, i*10)
		ma, _ := stats.Percentile(prices, (i+1)*10)

		pmin := int(mi)
		pmax := int(ma)

		labels = append(labels, int(pmax))
		pCount := 0
		sCount := 0
		lCount := 0

		for _, p := range details {

			if int(p.Item.Price) >= pmin && int(p.Item.Price) <= pmax && i == 0 {
				pCount++
				sCount += findMax(p.Item.Sold, p.Item.HistoricalSold)
				lCount += p.Item.LikedCount
			} else if int(p.Item.Price) > pmin && int(p.Item.Price) <= pmax {
				pCount++
				sCount += findMax(p.Item.Sold, p.Item.HistoricalSold)
				lCount += p.Item.LikedCount
			}
		}

		priceVal = append(priceVal, pCount)
		salesVal = append(salesVal, sCount)
		likeVal = append(likeVal, lCount)
	}

	analytics.PriceChart.Line.Labels = labels
	analytics.PriceChart.Line.ProductCount = priceVal
	analytics.PriceChart.Line.SalesCount = salesVal
	analytics.PriceChart.Line.LikesCount = likeVal

	return

}
