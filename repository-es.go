package main

import (
	"context"
	"log"
	"strconv"

	elastic "gopkg.in/olivere/elastic.v6"
)

func (a *App) EsInsertProductBulk(Products []EsProduct) (err error) {

	ctx := context.Background()
	bulkRequest := a.EsClient.Bulk()

	for _, p := range Products {

		req := elastic.NewBulkIndexRequest().
			Index("shopee-products").
			Type("_doc").Id(strconv.FormatInt(p.Itemid, 10)).
			Doc(p)

		bulkRequest = bulkRequest.Add(req)
	}

	_, err = bulkRequest.Do(ctx)
	if err != nil {
		log.Println(err)
		return
	}
	return

}
