package main

import (
	"log"
	"time"
)

func (a *App) BrandList() (timeStart time.Time, msg string, err error) {

	msg = "operation has already begun"
	timeStart = getTimeGMT()

	go a.InitiateMonitorShpMall()

	return
}

func (a *App) InitiateMonitorShpMall() {

	data := a.doBrandListCrawl()
	brandList := data.Data

	log.Println("brand found", len(brandList))

	numJobs := len(brandList)
	jobs := make(chan int, numJobs)
	results := make(chan []Item, numJobs)

	items := []Item{}

	t0 := time.Now()

	log.Println("crawling with worker", DEFAULT_WORKER)

	for w := 1; w <= DEFAULT_WORKER; w++ {
		go a.workerSearchShop(w, jobs, results)
	}

	for _, v := range brandList {
		jobs <- v.ShopID
	}

	for a := 1; a <= numJobs; a++ {
		items = append(items, <-results...)
	}

	a.proceedInsert(items, t0)

	close(jobs)

}

func (a *App) proceedInsert(items []Item, t0 time.Time) {

	viewCount := 0
	soldCount := 0
	possibleBot := 0.0

	log.Println("preparing data", len(items))
	histories := []ProductHistoryV2{}
	mapItem := make(map[int64]Item)

	for _, i := range items {

		viewCount += i.ViewCount
		soldCount += i.HistoricalSold

		if int(i.Price)%100 > 0 {
			possibleBot++
		}

		mapItem[i.ItemID] = i

	}

	for _, v := range mapItem {
		hist := MapProductToProductHistV2(v)
		histories = append(histories, hist)
	}

	histChunk := splitProductHistoryV2(histories, DEFAULT_DATA_CHUNK)

	log.Println("inserting data", len(histChunk), "X", DEFAULT_DATA_CHUNK)

	t1 := time.Now()

	for _, h := range histChunk {

		err := a.InsertProductHistoryBulkV2(h)
		if err != nil {
			log.Println(err)
		}

	}

	log.Println(len(mapItem), viewCount, soldCount, (possibleBot/float64(len(items)))*100, "%", time.Since(t0), time.Since(t1))

}

func (a *App) workerSearchShop(id int, jobs <-chan int, results chan<- []Item) {

	for j := range jobs {

		items, _ := a.sequentialPagination(j)
		results <- items
	}

}

func (a *App) sequentialPagination(shopID int) (items []Item, page int) {

	offset := 0
	page = 1

	for {

		sp := SearchParam{
			Matchid: shopID,
		}
		p := Param{
			Data:   sp,
			Offset: offset,
		}

		data := a.doSearchCrawl(p)
		items = append(items, data.Items...)

		if len(data.Items) < 50 {
			break
		}

		offset += 50
		page++
	}

	return
}
