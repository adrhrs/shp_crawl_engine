package main

import (
	"fmt"
	"log"
	"sort"
	"strconv"
	"time"
)

func (a *App) SalesChart(username string, start, finish string) (resp ResponseSalesLineChart, msg string, err error) {

	t0 := time.Now()

	seller, err := a.GetSellerInfo(username)
	if err != nil {
		return
	}

	t1 := time.Now()

	data, err := a.GetSellerSales(seller.ShopID, start, finish)
	if err != nil {
		return
	}

	t2 := time.Now()

	resp.ChartData = prepareSalesLineChart(data)
	resp.DataCount = len(data)

	t3 := time.Now()

	log.Println("Query Time", t1.Sub(t0), t2.Sub(t1), "Prepare Time", t3.Sub(t2))

	if len(resp.ChartData) > 0 {
		msg = "successfully curate sales data (" + strconv.Itoa(len(resp.ChartData)) + ")"
	} else {
		msg = "no available data from selected time range, pls check 3 hour later"
	}

	return
}

func prepareSalesLineChart(data []GetSalesAggregate) (result []SalesLineChart) {

	ids := make(map[int64]int)
	segmented := make(map[int64][]GetSalesAggregate)

	//get unique item_id
	for _, d := range data {
		ids[d.ItemID]++
	}

	//create smaller chunk based on item_id
	for k := range ids {
		arr := []GetSalesAggregate{}
		for _, d := range data {
			if k == d.ItemID {
				arr = append(arr, d)
			}
		}
		segmented[k] = arr
	}

	var revenue int

	mapRevPerTime := make(map[time.Time]int)
	mapItemsPerTime := make(map[time.Time]int)
	mapSoldPerTime := make(map[time.Time]int)
	mapLikePerTime := make(map[time.Time]int)
	mapRatingPerTime := make(map[time.Time]int)
	mapViewPerTime := make(map[time.Time]int)

	//iterate each id
	for id := range ids {

		//iterate each date
		for idx, d := range segmented[id] {

			if idx < len(segmented[id])-1 {
				soldDiff := d.HistoricalSold - segmented[id][idx+1].HistoricalSold // sold diffrence with previous time
				revenue = soldDiff * d.Price                                       // multiply with the price
				mapRevPerTime[d.UpdateTime] += revenue                             // accumulate revenue in time
				mapItemsPerTime[d.UpdateTime]++                                    // accumulate item count in time
				mapSoldPerTime[d.UpdateTime] += soldDiff                           // accumulate item count in time

				viewDiff := 0
				if segmented[id][idx+1].ViewCount > 0 {
					viewDiff = d.ViewCount - segmented[id][idx+1].ViewCount // view count diffrence with previous time
				}

				if viewDiff < 0 {
					viewDiff = 0 // avoiding surge of view count, still mysterious tho
				}

				likeDiff := d.LikedCount - segmented[id][idx+1].LikedCount     // view count diffrence with previous time
				ratingDiff := d.RatingCount - segmented[id][idx+1].RatingCount // view count diffrence with previous time

				mapLikePerTime[d.UpdateTime] += likeDiff     // accumulate item count in time
				mapRatingPerTime[d.UpdateTime] += ratingDiff // accumulate item count in time
				mapViewPerTime[d.UpdateTime] += viewDiff     // accumulate item count in time
			}

		}
	}

	//iterate each time
	for t, v := range mapRevPerTime {

		s := SalesLineChart{
			Time:        t,
			Revenue:     v,
			ItemCount:   mapItemsPerTime[t],
			ViewCount:   mapViewPerTime[t],
			LikeCount:   mapLikePerTime[t],
			SoldCount:   mapSoldPerTime[t],
			RatingCount: mapRatingPerTime[t],
			Label: fmt.Sprintf("%d-%02d-%02d %02d:%02d",
				t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute()),
		}

		result = append(result, s)
	}

	//sort map

	sort.Slice(result, func(i, j int) bool {
		return result[i].Time.Before(result[j].Time)
	})

	return

}

func (a *App) SellerHeader(username string) (resp ResponseSalesSummary, msg string, err error) {

	seller, err := a.GetSellerInfo(username)
	if err != nil {
		return
	}

	summary, err := a.GetSellerSummary(seller.ShopID)
	if err != nil {
		return
	}

	if len(summary) == 0 {
		msg = "historical data is not ready, get back in one hour"
		return
	}

	msg = "successful summarize seller"

	resp.Seller = seller
	resp.Product = summary[0]

	return
}
