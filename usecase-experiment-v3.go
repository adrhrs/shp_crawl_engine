package main

import (
	"log"
	"strconv"
	"strings"
	"time"
)

const (
	prefix = "shop:"
)

func (a *App) MainCategoryBasedCrawlV3(procedure string) {

	if procedure == "shop_by_cat_crawl" {
		a.MainCategoryBasedCrawlV3CrawlProcedure()
	} else if procedure == "shop_by_cat_save" {
		a.MainCategoryBasedCrawlV3SaveProcedure()
	} else if procedure == "shop_by_cat_all" {
		a.MainCategoryBasedCrawlV3CrawlProcedure()
		a.MainCategoryBasedCrawlV3SaveProcedure()
	} else if procedure == "product_by_shop_crawl" {
		a.MainProductByShopCrawlProcedure()
	} else {
		log.Println("wrong format")
	}

}

type WorkerSearchV4Param struct {
	SP   SearchParam
	Shop SellerProductCount
}

type WorkerSearchV4Return struct {
	SuccessInsert int
	EmptyShop     bool
	Shop          SellerProductCount
}

func (a *App) MainProductByShopCrawlProcedure() {
	limit := 1000
	worker := 96
	offset := 0
	batch := 1
	maxBatch := 420

	var (
		successRateAll              float64
		totalEmptyShop, crawledShop int
	)
	t0 := time.Now()
	for {
		t1 := time.Now()
		shops, err := a.GetSellerProductCount(0, offset, limit)
		if err != nil {
			log.Println(err)
		}

		if len(shops) == 0 {
			break
		}

		SPs := []WorkerSearchV4Param{}
		for _, s := range shops {
			SPs = append(SPs, WorkerSearchV4Param{
				SP: SearchParam{
					Matchid:    s.ShopID,
					WantedItem: 8000,
				},
				Shop: s,
			})
		}

		shops = []SellerProductCount{}
		numJobs := len(SPs)
		jobsChan := make(chan WorkerSearchV4Param, numJobs)
		resultChan := make(chan WorkerSearchV4Return, numJobs)

		var (
			insertedProduct int
			emptyShop       int
			successRate     float64
		)

		for w := 1; w <= worker; w++ {
			go a.workerSearchV4(w, jobsChan, resultChan)
		}

		for _, j := range SPs {
			jobsChan <- j
		}
		close(jobsChan)

		for a := 1; a <= numJobs; a++ {
			result := <-resultChan

			result.Shop.ProductCrawled = 1
			shops = append(shops, result.Shop)

			insertedProduct += result.SuccessInsert
			if result.EmptyShop {
				emptyShop++
			}
		}

		err = a.UpdateSellerProductCount(shops)
		if err != nil {
			log.Println(err)
			return
		}
		offset += limit
		batch++
		crawledShop += len(shops)
		if batch == maxBatch {
			break
		}
		if len(shops) > 0 {
			successRate = (float64(emptyShop) / float64(len(shops))) * 100
		}
		totalEmptyShop += emptyShop
		log.Println("finish crawl per batch", time.Since(t1), batch, "-", emptyShop, "/", len(shops), successRate)
	}
	if crawledShop > 0 {
		successRateAll = (float64(totalEmptyShop) / float64(crawledShop)) * 100
	}
	log.Println("finish crawl and store product by shop in", time.Since(t0), totalEmptyShop, "/", crawledShop, successRateAll)

}

func (a *App) MainCategoryBasedCrawlV3SaveProcedure() {
	t0 := time.Now()
	keys, err := a.ScanRedis("shop*")
	if err != nil {
		log.Println(err)
	}
	log.Println(len(keys), time.Since(t0), "get all keys")
	t1 := time.Now()
	var errRedisTotal, errDBTotal int
	for id, batch := range keys {
		SPCs := []SellerProductCount{}
		t2 := time.Now()
		for _, k := range batch {
			value, errRedis := a.GetRedis(k)
			if errRedis != nil {
				log.Println(errRedis)
				errRedisTotal++
			}
			SPCs = append(SPCs, SellerProductCount{
				ShopID: prepareShopID(k),
				ProductCount: func(old string) (new int) {
					new, _ = strconv.Atoi(old)
					return
				}(value),
				SourceCrawl: "category_based",
				UpdateTime:  getTimeGMT(),
			})
		}
		errDB := a.InsertSellerProductCountBulk(SPCs)
		if errDB != nil {
			log.Println(errDBTotal)
			errDBTotal++
		}
		log.Println(time.Since(t2), "finish insert batch", id, len(SPCs))
	}
	log.Println(time.Since(t1), "finish insert", errRedisTotal, errDBTotal)

}

func (a *App) MainCategoryBasedCrawlV3CrawlProcedure() {

	mapCat := make(map[int]int)
	SPs := []SearchParam{}
	cat := a.hitCategoryListAPI()
	t0 := time.Now()

	for _, lv1 := range cat {
		mapCat[lv1.Main.Catid] = 1
		for _, lv2 := range lv1.Sub {
			mapCat[lv2.Catid] = 2
			for _, lv3 := range lv2.SubSub {
				mapCat[lv3.Catid] = 3
			}
			break
		}
		break
	}

	for k := range mapCat {
		SPs = append(SPs, SearchParam{
			Matchid:    k,
			isCategory: true,
			WantedItem: 8000,
		})
	}

	numJobs := len(SPs)
	jobsChan := make(chan SearchParam, numJobs)
	successCountChan := make(chan int, numJobs)
	successCountAll := 0
	prog := 0

	for w := 1; w <= 16; w++ {
		go a.workerSearchV3(w, jobsChan, successCountChan)
	}

	for _, j := range SPs {
		jobsChan <- j
	}
	close(jobsChan)

	for a := 1; a <= numJobs; a++ {
		successCountAll += <-successCountChan
		prog++
		log.Println(prog, "/", len(SPs))
	}

	log.Println(len(mapCat), len(SPs), successCountAll, time.Since(t0))

}

//workerSearchV3 for search based category ID
func (a *App) workerSearchV3(id int, jobsChan <-chan SearchParam, successCountChan chan<- int) {
	for j := range jobsChan {
		uniqueShop := make(map[int]int)
		success := 0
		productByCat, _ := a.paginateProductCrawl(j)
		for _, p := range productByCat {
			uniqueShop[p.ShopID]++
		}
		for k, v := range uniqueShop {
			err := a.SetRedis(prefix+strconv.Itoa(k), v)
			if err != nil {
				log.Println(err)
			} else {
				success++
			}
		}
		successCountChan <- success
	}
}

//workerSearchV3 for search based category ID
func (a *App) workerSearchV4(id int, jobsChan <-chan WorkerSearchV4Param, resultChan chan<- WorkerSearchV4Return) {
	for j := range jobsChan {
		uniqueProduct := make(map[int64]Item)
		var (
			success int
			empty   bool
		)

		productByShop, _ := a.paginateProductCrawl(j.SP)
		for _, p := range productByShop {
			uniqueProduct[p.ItemID] = p
		}
		productByShop = []Item{}

		products := []ProductV2{}
		for _, p := range uniqueProduct {
			product := ProductV2{
				ItemID:         p.ItemID,
				ShopID:         p.ShopID,
				Name:           p.Name,
				CategoryID:     p.Catid,
				Price:          p.Price,
				HistoricalSold: p.HistoricalSold,
				UpdateTime:     getTimeGMT(),
			}
			products = append(products, product)
		}

		if len(products) > 0 {
			err := a.InsertProductsV2(products)
			if err != nil {
				log.Println(err)
			} else {
				success = len(uniqueProduct)
			}
		} else {
			empty = true
		}

		result := WorkerSearchV4Return{
			SuccessInsert: success,
			Shop:          j.Shop,
			EmptyShop:     empty,
		}

		resultChan <- result
	}
}

func prepareShopID(old string) int {
	stripped := strings.Replace(old, "shop:", "", -1)
	new, err := strconv.Atoi(stripped)
	if err != nil {
		log.Println(err)
	}
	return new
}
