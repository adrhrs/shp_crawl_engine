package main

import "time"

type Response struct {
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

type SearchResponse struct {
	Categories Category           `json:"categories"`
	Analytics  Analytics          `json:"analytics"`
	Products   []Detail           `json:"products"`
	Perfomance CrawlingPerfomance `json:"perfomance"`
}

type Category struct {
	CategoryCount int            `json:"category_count"`
	KeywordScore  float64        `json:"keyword_score"`
	Data          []CategoryData `json:"category_data"`
}

type CategoryData struct {
	Name         string  `json:"name"`
	ProductCount int     `json:"product_count"`
	SoldCount    int     `json:"sold_count"`
	Percentage   float64 `json:"percentage"`
	Color        string  `json:"color"`
}

type CrawlingPerfomance struct {
	SearchAPI  string `json:"search_api"`
	ProductAPI string `json:"product_api"`
	AllProcess string `json:"all_process"`
}

type Param struct {
	Data   SearchParam
	Offset int
}

type SearchParam struct {
	Keyword    string
	MinPrice   string
	WantedItem float64
	Matchid    int
	isCategory bool
	SortBy     string
}

type Analytics struct {
	PriceMean       int        `json:"price_mean"`
	PriceMedian     int        `json:"price_median"`
	PriceMax        int        `json:"price_max"`
	PriceMin        int        `json:"price_min"`
	PriceStdev      float64    `json:"price_stdev"`
	AllProductCount int        `json:"product_count"`
	ShopCount       int        `json:"shop_count"`
	PriceChart      PriceChart `json:"price_chart"`
	Prices          []float64  `json:"prices"`
}

type PriceChart struct {
	Line Line `json:"line"`
	Bar  Bar  `json:"bar"`
}

type Bar struct {
	Labels       []int `json:"labels"`
	ProductCount []int `json:"product_count"`
}
type Line struct {
	Labels       []int `json:"labels"`
	ProductCount []int `json:"product_count"`
	SalesCount   []int `json:"sales_count"`
	LikesCount   []int `json:"likes_count"`
}

type ResponseSellerProducts struct {
	ProductsCount int       `json:"products_count"`
	Products      []Product `json:"products"`
}

type ResponseSalesLineChart struct {
	DataCount int              `json:"data_count"`
	ChartData []SalesLineChart `json:"chart_data"`
}

type SalesLineChart struct {
	Time        time.Time `json:"timestamp"`
	ItemCount   int       `json:"item_count"`
	ViewCount   int       `json:"view_count"`
	LikeCount   int       `json:"like_count"`
	SoldCount   int       `json:"sold_count"`
	RatingCount int       `json:"rating_count"`
	Label       string    `json:"label"`
	Revenue     int       `json:"revenue"`
}

type ResponseSalesSummary struct {
	Product GetSummarySellerProduct `json:"product_summary"`
	Seller  Seller                  `json:"seller_summary"`
}

type PingResp struct {
	EachDuration  float64
	TotalDuration string
	TotalRequest  int
	TotalWorker   int
	MinItemID     int // if any 0 than some is fault
	Prove         interface{}
}

type PSSearchResult struct {
	Product         VasProductCrawl          `json:"product,omitempty"`
	ProductCount    int                      `json:"count,omitempty"`
	TotalProduct    float64                  `json:"total,omitempty"`
	CrawledProducts []Item                   `json:"crawled_products,omitempty"`
	Histories       []VasProductCrawlHistory `json:"histories,omitempty"`
}

type RevenueResponse struct {
	TotalRevenue int `json:"total_revenue"`
	TotalSold    int `json:"total_sold"`
	TotalReview  int `json:"total_review"`
	TotalView    int `json:"total_view"`
}
