package main

const (
	getSalesAggregate = `
	select
		item_id,
		historical_sold,
		view_count,
		liked_count,
		rating_count,
		price,
		update_time
	from
		product_histories
	where
		shop_id = (?) 
		and
		update_time >= (?)
		and
		update_time <= (?)
	order by
		item_id,update_time desc;
	`

	getSummarySellerProduct = `
	select
		sum(historical_sold*price) as revenue,
		count(*) as product_count,
		sum(view_count) as view_count,
		sum(liked_count) as liked_count,
		sum(historical_sold) as historical_sold,
		sum(rating_count) as rating_count,
		update_time,
		max(shop_id) as shop_id
	from
		product_histories
	where
		shop_id = (?)
	group by
		update_time
	order by
		update_time desc
	limit 1
	
	`
	getShopListWithStatistic = `
	select
		s.shop_id,
		s."name",
		s.username,
		coalesce(p.data_count, 0) as data_count,
		coalesce(p.first_date, NOW()) as first_date,
		coalesce(p.last_date, NOW()) as last_date,
		coalesce(s.follower_count, 0) as follower_count,
		coalesce(s.rating_good, 0) as rating_count,
		coalesce(t.product_count, 0) as product_count,
		coalesce(t.revenue, 0) as revenue
	from
		sellers s
	left join (
		select
			shop_id,
			count(*) as data_count,
			min(update_time) as first_date,
			max(update_time) as last_date
		from
			product_histories
		group by
			shop_id) as p on
		s.shop_id = p.shop_id
	left join (
		select
			count(*) as product_count,
			sum(historical_sold * price) as revenue,
			shop_id,
			update_time
		from
			product_histories
		where
			update_time = (
			select
				update_time
			from
				product_histories
			order by
				update_time desc
			limit 1)
		group by
			update_time,
			shop_id
		order by
			update_time desc

	) as t on
		t.shop_id = s.shop_id
	order by
		data_count;
	`

	getDistinctShopIDs = `
	select
		distinct shop_id
	from
		product_master_keys
	where
		shop_id not in (
		select
			distinct lv3_category_id
		from
			product_master_keys)
	`

	getUnDetailedProductKeys = `
	select
		*
	from
		product_master_keys
	where
		detail_status = 0
	limit 20000
	`

	getVasProductCrawlHistoriesOrdered = `
	select
		*
	from
		vas_product_crawl_histories
	where
		product_id = (?)
	order by
		to_char(update_time, 'YYYY-MM-DD HH24:MI') desc,
		item_order
	`
)

const (
	getSellerHistory = `
	select
		coalesce(shop_id,0) as shop_id,
		count(distinct item_id) as active_item,
		coalesce(min(price),0) as price_min,
		coalesce(max(price),0) as price_max,
		coalesce(sum(max_view),0) as view_count,
		coalesce(sum(max_sold),0) as sold_count,
		coalesce(sum(max_review),0) as review_count,
		coalesce(round(avg(avg_rating)::numeric,4),0) as rating_avg,
		update_time
	from
		(
		select
		(b.default_time::timestamp::date) as update_time,	
		*
		from
			(
			select
				item_id,
				min(shop_id) as shop_id,
				(update_time::timestamp::date) as tgl,
				min(price) as price,
				max(historical_sold) as max_sold,
				avg(rating_star) as avg_rating,
				max(rating_count) as max_review,
				max(view_count) as max_view
			from
				product_histories
			where
				shop_id = (?)
				and (update_time::timestamp::date)>= (?)
				and (update_time::timestamp::date)<= (?)
			group by
				item_id,
				(update_time::timestamp::date)
			order by
				(update_time::timestamp::date) asc ) a
		right join default_date b on
			a.tgl = (b.default_time::timestamp::date)
		where
			(default_time::timestamp::date)>= (?)
			and (default_time::timestamp::date)<= (?)) c
		group by update_time,shop_id;
	`

	getProductHistory = `
	select
		coalesce(a.item_id, 0) as item_id,
		(b.default_time::timestamp::date) as update_time,
		coalesce(avg_price, 0) as avg_price,
		coalesce(avg_stock, 0) as avg_stock,
		coalesce(max_sold, 0) as max_sold,
		coalesce(avg_rating, 0) as avg_rating,
		coalesce(max_review, 0) as max_review,
		coalesce(max_view, 0) as max_view
	from
		(
		select
			item_id,
			(update_time::timestamp::date) as tgl,
			min(price) as avg_price,
			avg(stock) as avg_stock ,
			max(historical_sold) as max_sold,
			avg(rating_star) as avg_rating,
			max(rating_count) as max_review,
			max(view_count) as max_view
		from
			product_histories
		where
			item_id = 1835009883
			and (update_time::timestamp::date)> '2020-06-30'
			and (update_time::timestamp::date)< '2020-09-25'
		group by
			item_id,
			(update_time::timestamp::date)
		order by
			(update_time::timestamp::date) asc ) a
	right join default_date b on
		a.tgl = (b.default_time::timestamp::date)
	where
		(default_time::timestamp::date)> '2020-06-30'
		and (default_time::timestamp::date)< '2020-09-25'
	;
	`
	getSellerRevenue = `
	select
		item_id,
		min(shop_id) as shop_id,
		(update_time::timestamp::date) as tgl,
		min(price) as price,
		max(historical_sold) as sold_count,
		max(rating_count) as review_count,
		max(view_count) as view_count
	from
		product_histories
	where
		shop_id = (?)
		and (update_time::timestamp::date) >= (?)
		and (update_time::timestamp::date) <= (?)
	group by
		item_id,
		(update_time::timestamp::date)
	order by
		item_id,
		(update_time::timestamp::date)

	`

	getSellerHistoryV3 = `
	select
		item_id,
		(update_time::timestamp::date) as update_time,
		min(price) as price,
		max(historical_sold) as sold_count,
		max(rating_count) as review_count,
		avg(rating_star) as rating_avg,
		max(view_count) as view_count
	from
		product_histories
	where
		shop_id = (?)
	group by
		item_id,
		(update_time::timestamp::date)
	order by
		item_id,
		(update_time::timestamp::date)
	`
)
