package main

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-redis/redis"
)

type keyByItemDate struct {
	ItemID     int64
	UpdateTime time.Time
}

type AggByDateReturn struct {
	Item   map[time.Time]int
	Sold   map[time.Time]int
	Review map[time.Time]int
	View   map[time.Time]int
	Rating map[time.Time]float64
}

type AggByDateParam struct {
	ByItemDate map[keyByItemDate]GetSellerHistoryV3
	ByItem     map[int64]int
	MinDate    time.Time
	MaxDate    time.Time
	AllDiff    int
}

func (a *App) SellerHistory(username, start, finish, historyType string) (result []GetSellerHistory, msg string, err error) {

	var (
		totalData int
		source    = "DB"
	)
	data, err := a.GetSellerInfo(username)
	if err != nil {
		return
	}

	t0 := time.Now()

	if historyType == "ALL_PRODUCT" {
		result, err = a.GetSellerHistoryV2(data.ShopID, start, finish)
		if err != nil {
			return
		}
		totalData = len(result)
	} else if historyType == "ACTIVE_PRODUCT" {
		var cache string
		key := fmt.Sprintf("%s_%s_%s", username, start, finish)
		cache, err = a.GetRedis(key)
		if err == redis.Nil {
			result, totalData, err = a.CurateHistoryDataFromDB(data, start, finish)
			if err != nil {
				return
			}
			err = a.SetRedis(key, result)
			if err != nil {
				return
			}
		} else {
			err = json.Unmarshal([]byte(cache), &result)
			if err != nil {
				return
			}
			source = "redis"
		}

	}

	msg = fmt.Sprintf("processed %d data for %d days, took %s, source %s",
		totalData, len(result), time.Since(t0).String(), source)

	return
}

func (a *App) CurateHistoryDataFromDB(data Seller, start, finish string) (result []GetSellerHistory, totalData int, err error) {

	ds, errDate := time.Parse("2006-01-02", start)
	if errDate != nil {
		err = errDate
		return
	}
	df, errDate := time.Parse("2006-01-02", finish)
	if errDate != nil {
		err = errDate
		return
	}
	diff := int(df.Sub(ds).Hours() / 24)

	hist, errHist := a.GetSellerHistoryV3(data.ShopID, start, finish)
	if errHist != nil {
		err = errHist
		return
	}

	preProcessed := preProcessHistData(hist)
	compiled := aggregateByDate(AggByDateParam{
		ByItem:     preProcessed.ByItem,
		ByItemDate: preProcessed.ByItemDate,
		MinDate:    preProcessed.MinDate,
		MaxDate:    preProcessed.MaxDate,
		AllDiff:    preProcessed.AllDiff,
	})

	for i := 1; i <= diff; i++ {
		d := ds.AddDate(0, 0, i)
		var (
			sold   = compiled.Sold[d]
			review = compiled.Review[d]
			view   = compiled.View[d]
			item   = compiled.Item[d]
			rating = compiled.Rating[d] / float64(item)
		)
		result = append(result, GetSellerHistory{
			ActiveItem:  item,
			SoldCount:   sold,
			RatingAvg:   rating,
			ReviewCount: review,
			ViewCount:   view,
			UpdateTime:  d,
		})
	}

	totalData = len(hist)

	return
}

func preProcessHistData(hist []GetSellerHistoryV3) (result AggByDateParam) {

	var (
		byItemDate       = make(map[keyByItemDate]GetSellerHistoryV3)
		byItem           = make(map[int64]int)
		minDate, maxDate time.Time
	)

	for id, h := range hist {
		if h.UpdateTime.Unix() >= maxDate.Unix() {
			maxDate = h.UpdateTime
		}
		if id > 0 {
			if h.UpdateTime.Unix() <= minDate.Unix() {
				minDate = h.UpdateTime
			}
		} else {
			minDate = h.UpdateTime
		}
		k := keyByItemDate{
			ItemID:     h.ItemID,
			UpdateTime: h.UpdateTime,
		}
		byItemDate[k] = h
		byItem[h.ItemID]++
	}

	allDiff := int(maxDate.Sub(minDate).Hours() / 24)
	result = AggByDateParam{
		ByItem:     byItem,
		ByItemDate: byItemDate,
		MinDate:    minDate,
		MaxDate:    maxDate,
		AllDiff:    allDiff,
	}

	return
}

func aggregateByDate(param AggByDateParam) (result AggByDateReturn) {

	var (
		itemByDate   = make(map[time.Time]int)
		soldByDate   = make(map[time.Time]int)
		reviewByDate = make(map[time.Time]int)
		ratingByDate = make(map[time.Time]float64)
		viewByDate   = make(map[time.Time]int)
		byItemDate   = param.ByItemDate
		byItem       = param.ByItem
		minDate      = param.MinDate
		allDiff      = param.AllDiff
	)

	for k, v := range byItem {
		var (
			lastSold   int
			lastReview int
			lastView   int
		)
		if v > 1 {
			for i := 0; i <= allDiff; i++ {

				var (
					soldDiff   int
					reviewDiff int
					viewDiff   int
				)

				d := minDate.AddDate(0, 0, i)
				key := keyByItemDate{
					ItemID:     k,
					UpdateTime: d,
				}
				currHist := byItemDate[key]

				if i > 0 {
					soldDiff = currHist.SoldCount - lastSold
					reviewDiff = currHist.ReviewCount - lastReview
					viewDiff = currHist.ViewCount - lastView
				}

				if currHist.SoldCount > 0 {
					lastSold = currHist.SoldCount
				}
				if currHist.ReviewCount > 0 {
					lastReview = currHist.ReviewCount
				}
				if currHist.ViewCount > 0 {
					lastView = currHist.ViewCount
				}

				if currHist.ItemID > 0 {
					itemByDate[d]++
				}
				if soldDiff > 0 {
					soldByDate[d] += soldDiff
				}
				if reviewDiff > 0 {
					reviewByDate[d] += reviewDiff
				}
				if viewDiff > 0 {
					viewByDate[d] += viewDiff
				}
				if currHist.RatingAvg > 0 {
					ratingByDate[d] += currHist.RatingAvg
				}
			}
		}
	}

	result = AggByDateReturn{
		Item:   itemByDate,
		Sold:   soldByDate,
		Review: reviewByDate,
		Rating: ratingByDate,
		View:   viewByDate,
	}
	return
}

func (a *App) SellerRevenue(username, start, finish string) (result RevenueResponse, msg string, err error) {
	data, err := a.GetSellerInfo(username)
	if err != nil {
		return
	}
	hist, err := a.GetSellerRevenue(data.ShopID, start, finish)
	if err != nil {
		return
	}

	byItem := make(map[int64][]GetSellerRevenue)

	for _, h := range hist {
		byItem[h.ItemID] = append(byItem[h.ItemID], h)
	}

	for _, v := range byItem {
		if len(v) > 1 {
			for i, d := range v {
				var soldDiff, reviewDiff, viewDiff, revenue int
				if i > 0 {
					soldDiff = d.SoldCount - v[i-1].SoldCount
					reviewDiff = d.ReviewCount - v[i-1].ReviewCount
					viewDiff = d.ViewCount - v[i-1].ViewCount
				}
				revenue = soldDiff * d.Price

				if revenue > 0 {
					result.TotalRevenue += revenue
				}

				if soldDiff > 0 {
					result.TotalSold += soldDiff
				}

				if reviewDiff > 0 {
					result.TotalReview += reviewDiff
				}

				if viewDiff > 0 {
					result.TotalView += viewDiff
				}
			}
		}
	}

	return
}
