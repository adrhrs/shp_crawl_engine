package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"strconv"
)

func (a *App) doSearchCrawl(p Param) (resp ShpRes) {

	url, par := buildSearchURL(p)
	req, err := http.NewRequest("GET", url, nil)
	req = addReqHeader(req, generateIfNoneMatch(par))
	if err != nil {
		log.Println(err)
	}

	res, err := a.Client.Do(req)
	err = json.NewDecoder(res.Body).Decode(&resp)
	if err != nil {
		log.Println(err)
	}

	defer res.Body.Close()

	return
}

func (a *App) doRouteSearch(q string) (resp FullSearchResp) {

	url := "https://shopee.co.id/api/v2/search_items/?" + q
	req, err := http.NewRequest("GET", url, nil)
	req = addReqHeader(req, generateIfNoneMatch(q))
	if err != nil {
		log.Println(err)
	}

	res, err := a.Client.Do(req)
	err = json.NewDecoder(res.Body).Decode(&resp)
	if err != nil {
		log.Println(err)
	}

	defer res.Body.Close()

	return
}

func (a *App) doDetailCrawl(d Item) (resp Detail) {

	it := strconv.FormatInt(d.ItemID, 10)
	sh := strconv.Itoa(d.ShopID)
	url := "https://shopee.co.id/api/v2/item/get?itemid=" + it + "&shopid=" + sh
	par := "itemid=" + it + "&shopid=" + sh

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Println(err)
	}

	req = addReqHeader(req, par)

	res, err := a.Client.Do(req)
	err = json.NewDecoder(res.Body).Decode(&resp)
	if err != nil {
		log.Println(err)
	}

	defer res.Body.Close()

	return

}

func (a *App) doSellerCrawl(username string) (resp SellerInfo) {

	url := "https://shopee.co.id/api/v2/shop/get?username=" + username

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Println(err)
	}

	req = addReqHeader(req, "")
	res, err := a.Client.Do(req)

	err = json.NewDecoder(res.Body).Decode(&resp)
	if err != nil {
		log.Println(err)
	}

	defer res.Body.Close()

	return
}

func (a *App) doBrandListCrawl() (resp BrandResp) {
	url := "https://shopee.co.id/api/v2/brand_lists/get"
	method := "GET"

	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		fmt.Println(err)
	}

	res, err := client.Do(req)
	err = json.NewDecoder(res.Body).Decode(&resp)
	if err != nil {
		log.Println(err)
	}

	defer res.Body.Close()

	return
}

func (a *App) hitCategoryListAPI() (resp CategoryResp) {
	url := "https://shopee.co.id/api/v1/category_list/"
	method := "GET"

	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("Cookie", "SPC_IA=-1; SPC_EC=-; SPC_F=35NBUoMm6mXoEbGQW0OQubjmrh1O00Qw; REC_T_ID=2649038c-da86-11e9-af64-3c15fb3aefa4; SPC_T_ID=\"GfukhIVsvpirqGUBobyG95ntAVCG7FZKvYZERYkJIRNFWeLYjSzyiBHTmumRmOPumr5dbXwLmBQtt6E3x595HI6o+SdKppSLl48v82RNsGQ=\"; SPC_U=-; SPC_T_IV=\"QdJUDY4RoiUpDQkekTfalg==\"; SPC_SI=fyaz9ei66qw0xoyxisc0v0zr08eo0h1o; SPC_CT_0ccf9883=\"1593063811.Wa7/Pl4niu/QdQGLvHO412OHMER0OMT1w/k+SZMT4qOIhC8uHlajJiVM4A2YvOx1\"; SPC_CT_fbdd7a7f=\"1593063977.RbPhmPB7smprzVDTMlPREDCdmHKkCZ6Ik75zUAvnAGg=\"; SPC_R_T_ID=\"nh4BB24EyXhIANK1iDNSPwap3r0K9ao5v97dH4R86N0kJJ2h0LeC3WhIfdpkEYTg4ff4V0F3F562fsLEhKbogU40fdEmPbBfyaLWgIL8CXI=\"; SPC_R_T_IV=\"BgBpO/R90zR3HdvZgdi8aA==\"")

	res, err := client.Do(req)

	err = json.NewDecoder(res.Body).Decode(&resp)
	if err != nil {
		log.Println(err)
	}

	defer res.Body.Close()

	return
}

func (a *App) doSearchShop(shopname string) (resp ShopSearchResp) {

	shopname = url.QueryEscape(shopname)
	url := "https://shopee.co.id/api/v2/search_users/?keyword=" + shopname + "&limit=1&offset=0&page=search_user&with_search_cover=true"
	method := "GET"

	client := &http.Client{}
	req, err := http.NewRequest(method, url, nil)

	if err != nil {
		fmt.Println(err)
	}
	req.Header.Add("authority", "shopee.co.id")
	req.Header.Add("x-shopee-language", "id")
	req.Header.Add("x-requested-with", "XMLHttpRequest")
	req.Header.Add("if-none-match-", "55b03-7b89c53ef055790b9d2b6a87c626892f")
	req.Header.Add("user-agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.113 Safari/537.36")
	req.Header.Add("x-api-source", "pc")
	req.Header.Add("accept", "*/*")
	req.Header.Add("sec-fetch-site", "same-origin")
	req.Header.Add("sec-fetch-mode", "cors")
	req.Header.Add("sec-fetch-dest", "empty")
	req.Header.Add("referer", "https://shopee.co.id/search_user?keyword=art%20%26%20sew%20co.")
	req.Header.Add("accept-language", "en-US,en;q=0.9")
	res, err := client.Do(req)
	err = json.NewDecoder(res.Body).Decode(&resp)
	if err != nil {
		log.Println(err)
	}

	defer res.Body.Close()

	return
}
