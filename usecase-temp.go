package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/masatana/go-textdistance"
	"github.com/montanaflynn/stats"
)

func (a *App) cek() {

	fmt.Println("\n", "--------")

	file, _ := os.Create("comp.csv")
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	f, _ := os.Open("result.csv")
	r := csv.NewReader(f)
	t0 := time.Now()
	id := 0
	dist := []float64{}
	notmatch := 0
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		tkpdName := record[0]
		data := a.doSearchShop(tkpdName)

		// name, shopid, domain,  total product, follower, distance
		row := []string{tkpdName + "|NOT MATCH|0|-|0|0|0"}

		if len(data.Data.Users) > 0 {
			d := data.Data.Users[0]
			t := strings.ToLower(tkpdName)
			s := strings.ToLower(d.Shopname)
			di := textdistance.JaroWinklerDistance(t, s)

			idstr := strconv.Itoa(d.Shopid)
			tot := strconv.Itoa(d.Products)
			fol := strconv.Itoa(d.FollowerCount)
			dd := strconv.FormatFloat(di, 'f', 2, 64)

			row = []string{tkpdName + "|" + d.Shopname + "|" + idstr +
				"|" + d.Username + "|" + tot + "|" + fol + "|" + dd}

			dist = append(dist, di)
		} else {
			notmatch++
		}

		fmt.Println(tkpdName, id)
		id++
		writer.Write(row)

	}

	avgdist, _ := stats.Mean(dist)

	fmt.Println("\n", "--------", avgdist, notmatch, time.Since(t0))

}
