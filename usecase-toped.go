package main

import (
	"log"
)

func (a *App) PSGetSearchResult(productIDs []int, isHistory bool, triggerCron bool) (data []PSSearchResult, err error) {

	if triggerCron {

		go a.PSCronSearch()
		data[0].Product.ProductTitle = "trigger success"

	} else if isHistory {

		if productIDs[0] == 0 {

			productIDs = []int{}
			pr, _ := a.GetVasProductCrawl()
			for _, p := range pr {
				productIDs = append(productIDs, p.ProductID)
			}
		}

		for _, p := range productIDs {

			r := PSSearchResult{}
			product, errP := a.GetVasProductCrawlByPID(p)
			if errP != nil {
				err = errP
				log.Println(err)
				return
			}
			cps, errP := a.GetVasProductCrawlHistoryByProductID(product.ProductID)
			if errP != nil {
				err = errP
				log.Println(err)
				return
			}

			r.Product = product
			r.Histories = cps
			r.ProductCount = len(cps)

			data = append(data, r)
		}
	}

	return
}

func (a *App) PSCronSearch() {
	products, _ := a.GetVasProductCrawl()
	for _, pr := range products {

		cps, _ := a.paginateProductCrawl(SearchParam{
			Keyword:    pr.NormalizedName,
			MinPrice:   "0",
			WantedItem: 120,
		})

		histories := []VasProductCrawlHistory{}

		for _, cp := range cps {
			hist := VasProductCrawlHistory{
				ProductID:  pr.ProductID,
				ItemID:     cp.ItemID,
				ItemName:   cp.Name,
				ItemURL:    constructURL(cp.Name, cp.ItemID, cp.ShopID),
				ItemOrder:  cp.Order,
				UpdateTime: getTimeGMT(),
			}

			histories = append(histories, hist)
		}

		err := a.InsertVasProductCrawlHistoryBulk(histories)
		if err != nil {
			log.Println(err)
		}

	}
}
