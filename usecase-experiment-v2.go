package main

import (
	"encoding/csv"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

type HammerTestResponse struct {
	TotalData   int
	TotalActual int
	Dur         string
	DurMs       int64
	Worker      int
	NumJobs     int
}

type HeavyData struct {
	Worker    int
	TotalData int
	DurMs     int64
}

func (a *App) SearchAPIHammerTest(worker, numJobs int, keyword string) (data HammerTestResponse) {

	p := Param{}
	p.Data = SearchParam{
		Keyword:  keyword,
		MinPrice: "0",
		SortBy:   "sales",
	}
	p.Offset = 0

	ShopeeResult := []ShpRes{}
	totalData := 0

	t0 := time.Now()

	jobs := make(chan Param, numJobs)
	results := make(chan ShpRes, numJobs)

	for w := 1; w <= worker; w++ {
		go a.workerGetSearchResult(w, jobs, results)
	}

	for i := 0; i < numJobs; i++ {
		jobs <- p
	}

	for a := 1; a <= numJobs; a++ {
		r := <-results
		ShopeeResult = append(ShopeeResult, r)
		totalData += len(r.Items)
	}

	data.TotalData = totalData
	data.TotalActual = ShopeeResult[0].TotalCount
	data.Dur = time.Since(t0).String()
	data.DurMs = int64(time.Since(t0) / time.Millisecond)

	data.Worker = worker
	data.NumJobs = numJobs

	return

}

func (a *App) workerGetSearchResult(id int, jobs <-chan Param, results chan<- ShpRes) {

	for j := range jobs {
		results <- a.doSearchCrawl(j)
	}

}

func (a *App) HeavyHammerTest(freq, minWorker, maxWorker, numJobs int, keyword string) {

	go func() {
		data := []HeavyData{}
		prefix := strconv.Itoa(minWorker) + "_" + strconv.Itoa(maxWorker) + "_" + strconv.Itoa(numJobs) + "_" + strconv.Itoa(freq) + "_" + keyword + "_"
		for i := minWorker; i <= maxWorker; i++ {
			for j := 0; j < freq; j++ {
				d := a.SearchAPIHammerTest(i, numJobs, keyword)
				h := HeavyData{
					Worker:    i,
					DurMs:     d.DurMs,
					TotalData: d.TotalData,
				}
				data = append(data, h)
				log.Println(j, h)
			}
		}

		system, err := filepath.Abs("./")
		if err != nil {
			log.Println(err)
			return
		}
		folder := system + "/hammer_result/"
		file, _ := os.Create(folder + prefix + "result.csv")
		defer file.Close()

		writer := csv.NewWriter(file)
		defer writer.Flush()

		head := []string{"worker", "dur_ms", "tot_data"}
		writer.Write(head)
		for _, d := range data {
			row := []string{strconv.Itoa(d.Worker), strconv.FormatInt(d.DurMs, 10), strconv.Itoa(d.TotalData)}
			writer.Write(row)
		}
	}()

	return
}
