package main

const (
	STATIC_DIR = "/static/"

	DEFAULT_PAGE_SIZE  = 50
	DEFAULT_WORKER     = 32
	DEFAULT_DATA_CHUNK = 10000
)

var (
	APP_PORT = ":9898"
	// TIME_SPAN = []int{0, 6, 12, 18}
	TIME_SPAN = []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23}
)
