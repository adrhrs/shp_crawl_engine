package main

import (
	"log"
	"net/http"
	"time"

	pg "github.com/go-pg/pg"
	"github.com/go-redis/redis"
	"github.com/gorilla/mux"
	elastic "gopkg.in/olivere/elastic.v6"
)

func (a *App) initRouter() *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/api/health", IndexHandler)
	r.HandleFunc("/api/shp/search", a.SearchHandler) //hit SHP API

	r.HandleFunc("/api/shp/seller-info", a.SellerInfoHandler)         //hit DB if exist, if not crawl and insert
	r.HandleFunc("/api/shp/seller-products", a.SellerProductsHandler) //hit DB if exist, if not crawl and insert
	r.HandleFunc("/api/shp/seller-list", a.SellerListHandler)         //hit DB pretty tough query it is
	r.HandleFunc("/api/v2/shp/seller-list", a.SellerListHandlerV2)    //hit DB lightweight version
	r.HandleFunc("/api/shp/seller-history", a.SellerHistoryHandler)   //hit DB pretty tough query it is
	r.HandleFunc("/api/shp/seller-revenue", a.SellerRevenueHandler)   //hit DB pretty tough query it is

	r.HandleFunc("/api/shp/chart/seller", a.SellerChartHandler)   //hit DB if exist, if not crawl and insert
	r.HandleFunc("/api/shp/header/seller", a.SellerHeaderHandler) //hit DB if exist, if not crawl and insert

	r.HandleFunc("/api/shp/brand/list", a.BrandListHandler)       // get shopee brand list from API
	r.HandleFunc("/api/shp/category/list", a.CategoryListHandler) // get shopee hit perfomance

	//test api
	r.HandleFunc("/api/shp/ping-detail", a.pingDetailHandler)                           // get shopee hit perfomance
	r.HandleFunc("/api/shp/master-product-project", a.MasterProductProjectHandler)      // get shopee product all
	r.HandleFunc("/api/v3/shp/master-product-project", a.MasterProductProjectHandlerV3) // get shopee product all v2
	r.HandleFunc("/api/shp/search-hammer", a.SearchAPIHammerTestHandler)                // get shopee search hit perfomance
	r.HandleFunc("/api/shp/search-hammer-heavy", a.HeavyHammerTestHandler)              // get shopee search hit perfomance with heavy duty

	// route api
	r.HandleFunc("/api/v2/search_items/", a.RouteSearchHandler) // get shopee hit perfomance
	r.HandleFunc("/api/ps/search", a.PSGetSearchResultHandler)  // get shopee hit perfomance

	r.Use(loggingMiddleware)

	return r
}

func loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		t1 := time.Now()
		next.ServeHTTP(w, r)
		t2 := time.Now()
		log.Println(r.Method, t2.Sub(t1))
	})
}

func (a *App) initDB() *pg.DB {

	dbName := "shp_crawler_db"

	db := pg.Connect(&pg.Options{
		User:     "localuser",
		Password: "localpass",
		Database: dbName,
	})

	log.Println("successfully init DB \t ", dbName)

	return db
}

func (a *App) initCron() {

	rules := []string{"1 * * * *",
		"@never", "5 0 * * *", "1/1 * * * *"}

	a.Cron.AddFunc(rules[1], a.RegisterCronInfo)
	a.Cron.AddFunc(rules[1], a.RegisterCronMonitorShpMall)
	a.Cron.AddFunc(rules[1], a.RegisterCronCategoryBasedCrawl)
	a.Cron.AddFunc(rules[1], a.RegisterCronShopBasedCrawl)
	a.Cron.AddFunc(rules[1], a.RegisterCronDetailCrawl)
	a.Cron.AddFunc(rules[1], a.RegisterCronPSCronSearch)
	a.Cron.Start()

	log.Println("successfully init Cron \t ", rules)

}

func (a *App) initRedis() *redis.Client {

	address := "127.0.0.1:6379"

	client := redis.NewClient(&redis.Options{
		Addr:     address,
		Password: "",
		DB:       0,
	})

	log.Println("successfully init Redis \t ", address)

	return client
}

func (a *App) initES() *elastic.Client {

	address := "http://127.0.0.1:9200"

	client, err := elastic.NewClient(elastic.SetURL(address))
	if err != nil {
		log.Println(err)
	}

	a.EsClient = client

	log.Println("successfully init ES \t ", address)

	return a.EsClient
}
