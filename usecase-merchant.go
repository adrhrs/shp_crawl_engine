package main

import (
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/go-pg/pg"
)

func Index() string {
	return "server is good"
}

///// GET SELLER INFO /////

func (a *App) SellerInfo(username string) (seller Seller, msg string, err error) {

	msg = "retrive from DB"
	seller, err = a.GetSellerInfo(username)
	if err == pg.ErrNoRows {
		seller, err = a.SearchSeller(username)
		if err != nil {
			return
		}
		err = nil
		msg = "retrieve from Shopee API"
	}

	// override update time with create time
	seller.UpdateTime = time.Unix(int64(seller.Ctime), 0)

	return
}

func (a *App) SellerListV2() (sellers []Seller, msg string, err error) {

	msg = "retrieve from DB"
	sellers, err = a.GetSellerList()
	if err != nil {
		return
	}

	return
}

func (a *App) SellerList() (data []GetShopListWithStatistic, msg string, err error) {

	msg = "retrieve from DB, sorry query little tough"
	sellerData, err := a.GetSellerListV2()
	if err != nil {
		return
	}

	for _, d := range sellerData {

		t := d.FirstDate
		diff := d.LastDate.Sub(d.FirstDate)
		d.FirstDateFormatted = fmt.Sprintf("%d-%02d-%02d %02d:%02d (%dd)",
			t.Year(), t.Month(), t.Day(),
			t.Hour(), t.Minute(), int(diff.Hours()/24))

		data = append(data, d)
	}

	return
}

func (a *App) SearchSeller(username string) (seller Seller, err error) {

	log.Println("crawling seller ", username)

	sellerInfo := a.doSellerCrawl(username)
	if sellerInfo.Error == 4 {
		err = errors.New("not found on shopee")
		log.Println(err)
		return
	}

	seller = mapSellerInfoToSeller(sellerInfo)
	err = a.InsertSellerInfo(seller)
	if err != nil {
		return
	}

	_, err = a.MarkSellerHistory(seller)
	if err != nil {
		return
	}

	return
}

func (a *App) MarkSellerHistory(seller Seller) (history SellerHistory, err error) {

	history = mapSellerToSellerHistory(seller)
	err = a.InsertSellerHistory(history)
	if err != nil {
		return
	}

	return
}

///// GET SELLER PRODUCT //////

func (a *App) SellerProducts(username string) (resp ResponseSellerProducts, msg string, err error) {

	msg = "retrieve from DB"

	seller, err := a.GetSellerInfo(username)
	if err != nil {
		return
	}

	products, err := a.GetSellerProducts(seller.ShopID)
	if err != nil {
		return
	}

	if len(products) == 0 {

		_, err = a.SearchSellerProduct(seller)
		if err != nil {
			return
		}

		products, err = a.GetSellerProducts(seller.ShopID)
		if err != nil {
			return
		}

		msg = "retrieve from API"
	}

	resp.Products = products
	resp.ProductsCount = len(products)

	return
}

func (a *App) SearchSellerProduct(seller Seller) (products []Product, err error) {

	log.Println("crawling products for", seller.Username)

	sp := SearchParam{
		Matchid:    seller.ShopID,
		WantedItem: float64(seller.ItemCount),
	}

	itemIDs, _ := a.paginateProductCrawl(sp)
	items := a.poolGetProductDetail(itemIDs)

	log.Println("inserting products for", seller.Username)

	for _, i := range items {

		pr := MapDetailToProduct(i)
		err = a.InsertProduct(pr)
		if err != nil {
			continue
		}
		_, err := a.MarkProductHistory(pr)
		if err != nil {
			continue
		}

		products = append(products, pr)
	}

	return
}

func (a *App) MarkProductHistory(product Product) (history ProductHistory, err error) {

	history = MapProductToProductHist(product)
	err = a.InsertProductHistory(history)
	if err != nil {
		return
	}

	return
}
