package main

import "time"

type Seller struct {
	ID               int       `json:"id" db:"id"`
	UserID           int       `json:"userid" db:"userid"`
	ShopID           int       `json:"shopid" db:"shopid"`
	Name             string    `json:"name" db:"name"`
	Portrait         string    `json:"portrait" db:"portrait"`
	IsOfficialShop   bool      `json:"is_official_shop" db:"is_official_shop"`
	Description      string    `json:"description" db:"description"`
	Place            string    `json:"place" db:"place"`
	Username         string    `json:"username" db:"username"`
	ItemCount        int       `json:"item_count" db:"item_count"`
	CancellationRate int       `json:"cancellation_rate" db:"cancellation_rate"`
	FollowerCount    int       `json:"follower_count" db:"follower_count"`
	RatingGood       int       `json:"rating_good" db:"rating_good"`
	ResponseRate     int       `json:"response_rate" db:"response_rate"`
	PreparationTime  int       `json:"preparation_time" db:"preparation_time"`
	ResponseTime     int       `json:"response_time" db:"response_time"`
	Ctime            int       `json:"ctime" db:"ctime"`
	RatingStar       float64   `json:"rating_star" db:"rating_star"`
	LastActiveTime   int       `json:"last_active_time" db:"last_active_time"`
	UpdateTime       time.Time `json:"update_time" db:"update_time"`
}

type SellerHistory struct {
	ID              int       `json:"id" db:"id"`
	ShopID          int       `json:"shopid" db:"shopid"`
	Username        string    `json:"username" db:"username"`
	ItemCount       int       `json:"item_count" db:"item_count"`
	FollowerCount   int       `json:"follower_count" db:"follower_count"`
	RatingGood      int       `json:"rating_good" db:"rating_good"`
	ResponseRate    int       `json:"response_rate" db:"response_rate"`
	PreparationTime int       `json:"preparation_time" db:"preparation_time"`
	ResponseTime    int       `json:"response_time" db:"response_time"`
	RatingStar      float64   `json:"rating_star" db:"rating_star"`
	UpdateTime      time.Time `json:"update_time" db:"update_time"`
}

type Product struct {
	ID             int       `json:"id" db:"id"`
	ItemID         int64     `json:"item_id" db:"item_id"`
	ShopID         int       `json:"shop_id" db:"shop_id"`
	Name           string    `json:"name" db:"name"`
	Brand          string    `json:"brand" db:"brand"`
	CategoryName   string    `json:"category_name" db:"category_name"`
	CategoryID     string    `json:"category_id" db:"category_id"`
	Images         string    `json:"images" db:"images"`
	Description    string    `json:"description" db:"description"`
	Stock          int       `json:"stock" db:"stock"`
	Price          float64   `json:"price" db:"price"`
	HistoricalSold int       `json:"historical_sold" db:"historical_sold"`
	RatingStar     float64   `json:"rating_star" db:"rating_star"`
	RatingCount    int       `json:"rating_count" db:"rating_count"`
	LikedCount     int       `json:"liked_count" db:"liked_count"`
	ViewCount      int       `json:"view_count" db:"view_count"`
	UpdateTime     time.Time `json:"update_time" db:"update_time"`
}

type ProductHistory struct {
	ID             int       `json:"id" db:"id"`
	ItemID         int64     `json:"item_id" db:"item_id"`
	ShopID         int       `json:"shop_id" db:"shop_id"`
	Stock          int       `json:"stock" db:"stock"`
	Price          float64   `json:"price" db:"price"`
	HistoricalSold int       `json:"historical_sold" db:"historical_sold"`
	RatingStar     float64   `json:"rating_star" db:"rating_star"`
	RatingCount    int       `json:"rating_count" db:"rating_count"`
	LikedCount     int       `json:"liked_count" db:"liked_count"`
	ViewCount      int       `json:"view_count" db:"view_count"`
	UpdateTime     time.Time `json:"update_time" db:"update_time"`
}

type ProductHistoryV2 struct {
	ID             int       `json:"id" db:"id"`
	ItemID         int64     `json:"item_id" db:"item_id"`
	ShopID         int       `json:"shop_id" db:"shop_id"`
	Stock          int       `json:"stock" db:"stock"`
	Price          float64   `json:"price" db:"price"`
	HistoricalSold int       `json:"historical_sold" db:"historical_sold"`
	RatingStar     float64   `json:"rating_star" db:"rating_star"`
	RatingCount    int       `json:"rating_count" db:"rating_count"`
	LikedCount     int       `json:"liked_count" db:"liked_count"`
	ViewCount      int       `json:"view_count" db:"view_count"`
	UpdateTime     time.Time `json:"update_time" db:"update_time"`
}

type ProductMasterKey struct {
	ID            int       `json:"id" db:"id"`
	ItemID        int64     `json:"item_id" db:"item_id"`
	ShopID        int       `json:"shop_id" db:"shop_id"`
	SourceCrawl   string    `json:"source_crawl" db:"source_crawl"`
	Lv3CategoryID int       `json:"lv3_category_id" db:"lv3_category_id"`
	DetailStatus  int       `json:"detail_status" db:"detail_status"`
	UpdateTime    time.Time `json:"update_time" db:"update_time"`
}

type VasProductCrawl struct {
	ID              int    `json:"id" db:"id"`
	ProductID       int    `json:"product_id" db:"product_id"`
	ProductTitle    string `json:"product_title" db:"product_title"`
	Price           int    `json:"price" db:"price"`
	DiscountedPrice int    `json:"discounted_price" db:"discounted_price"`
	ProductURL      string `json:"product_url" db:"product_url"`
	NormalizedName  string `json:"normalized_name" db:"normalized_name"`
}

type VasProductCrawlHistory struct {
	ID         int       `json:"id" db:"id"`
	ProductID  int       `json:"product_id" db:"product_id"`
	ItemID     int64     `json:"item_id" db:"item_id"`
	ItemName   string    `json:"item_name" db:"item_name"`
	ItemURL    string    `json:"item_url" db:"item_url"`
	ItemOrder  int       `json:"item_order" db:"item_order"`
	UpdateTime time.Time `json:"update_time" db:"update_time"`
}

type SellerProductCount struct {
	ID             int       `json:"id" db:"id"`
	ShopID         int       `json:"shop_id" db:"shop_id"`
	ProductCount   int       `json:"product_count" db:"product_count"`
	ProductCrawled int       `json:"product_crawled" db:"product_crawled"`
	SourceCrawl    string    `json:"source_crawl" db:"source_crawl"`
	UpdateTime     time.Time `json:"update_time" db:"update_time"`
}

type ProductV2 struct {
	ID             int       `json:"id" db:"id"`
	ItemID         int64     `json:"item_id" db:"item_id"`
	ShopID         int       `json:"shop_id" db:"shop_id"`
	Name           string    `json:"name" db:"name"`
	CategoryID     int       `json:"category_id" db:"category_id"`
	Price          float64   `json:"price" db:"price"`
	HistoricalSold int       `json:"historical_sold" db:"historical_sold"`
	UpdateTime     time.Time `json:"update_time" db:"update_time"`
}
