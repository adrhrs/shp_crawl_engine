package main

import (
	"log"
	"net/http"
	"time"

	pg "github.com/go-pg/pg"
	"github.com/go-redis/redis"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/robfig/cron"
	elastic "gopkg.in/olivere/elastic.v6"
)

//App is app struct
type App struct {
	Router   *mux.Router
	Client   *http.Client
	DB       *pg.DB
	Cron     *cron.Cron
	Redis    *redis.Client
	EsClient *elastic.Client
}

func (a *App) init() {

	t0 := time.Now()

	a.Client = &http.Client{}
	a.Cron = cron.New()
	a.Router = a.initRouter()
	a.DB = a.initDB()
	a.Redis = a.initRedis()
	// a.EsClient = a.initES()

	a.initCron()

	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Authorization", "Content-Type"})
	corsObj := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

	// a.cek()

	log.Printf("Ready to rock and roll  \t  %s ", APP_PORT)
	log.Println()
	log.Printf("------------------- %s ------------------- ", time.Since(t0))
	log.Fatal(http.ListenAndServe(APP_PORT, handlers.CORS(headersOk, methodsOk, corsObj)(a.Router)))

}

func main() {

	a := App{}
	a.init()

}

type Test struct {
	Name   string `json:"name"`
	ShopID int64  `json:"shopid"`
}
