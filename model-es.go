package main

type EsProduct struct {
	Itemid     int64  `json:"itemid"`
	Name       string `json:"name"`
	Price      int    `json:"price"`
	Categories []struct {
		DisplayName string `json:"display_name"`
		Catid       int    `json:"catid"`
	} `json:"categories"`
	Description string `json:"description"`
}
